import React, { useState, useEffect } from 'react'
import isEmpty from 'lodash/isEmpty'
import { ToastContainer, toast } from 'react-toastify'

// Components
import HeaderArea from '../Components/HeaderArea'
import AppWrapper from '../Components/AppWrapper'
import CompanyFilter from '../Components/CompanyDropdownFilter'
import AddNewContentModal from '../Components/WordingContent/AddNewContentModal'
import WordingContentRendezvous from '../Components/WordingContent/WordingContentRendezvous'
import Pagination from '../Components/PaginationHooks'

// Fetching Group
import { fetchGetWordingContent, fetchInsertWordingContent } from '../Services/FetchRequestGroup'

const $ = window.$

export default function WordingContentPage (props) {
  const [isLoading, setIsLoading] = useState(false)
  const [offset, setOffset] = useState(0)
  const [syncCurrentOffset, setSyncCurrentOffset] = useState(0)
  const [page, setPage] = useState(parseInt(offset) ? (offset / 10 + 1) : 1)
  const [success, setSuccess] = useState({ success: false, data: {} })
  const [searchContent, setSearchContent] = useState('')
  const [listWordingContent, setListWordingContent] = useState({})
  const [filterCompanyCode, setFilterCompanyCode] = useState('All')
  const [title, setTitle] = useState('')
  const [companyCode, setCompanyCode] = useState('All')
  const [location, setLocation] = useState('')
  const [mobileWording, setMobileWording] = useState('')
  const [desktopWording, setDesktopWording] = useState('')
  const [globalWording, setGlobalWording] = useState('')
  const [platform, setPlatform] = useState('Mobile & Desktop')
  const [selectedPlatform, setSelectedPlatform] = useState(0)

  useEffect(() => {
    getListWordingContent()
  }, [])

  useEffect(() => {
    getListWordingContent()
  }, [filterCompanyCode])

  useEffect(() => {
    if (success.success) {
      setSuccess({ success: false, data: {} })
      getListWordingContent(syncCurrentOffset)
    }
  }, [success])

  function getListWordingContent (offsetProps) {
    let data = {
      company: filterCompanyCode
    }
    if (offsetProps) {
      data = { ...data, offset: offsetProps }
    } else {
      data = { ...data, offset: 0 }
    }
    if (!isEmpty(searchContent)) {
      data = { ...data, search: searchContent }
    }
    fetchGetWordingContent(data, setListWordingContent, setIsLoading, null)
  }

  function searchingWordingContent (searchedWord) {
    if (searchedWord === '') {
      window.location.reload()
    } else {
      const data = {
        company: filterCompanyCode,
        search: searchedWord
      }
      fetchGetWordingContent(data, setListWordingContent, setIsLoading, null)
    }
  }

  function changeCompanyCode (company, changeFrom) {
    if (changeFrom === 'header') {
      setFilterCompanyCode(company)
    } else if (changeFrom === 'addContentModal') {
      setCompanyCode(company)
    }
  }

  const handleInput = (stateSetter, type) => {
    if (type === 'jodit') {
      return e => {
        stateSetter(e)
      }
    } else {
      return e => {
        stateSetter(e.target.value)
      }
    }
  }

  function errorMsgDetection () {
    let emptyFieldDetection
    if (isEmpty(title)) {
      emptyFieldDetection = 'TITLE'
    } else if (isEmpty(location)) {
      emptyFieldDetection = 'LOCATION'
    } else {
      if (platform === 'Mobile & Desktop' && isEmpty(globalWording)) {
        emptyFieldDetection = 'WORDING CONTENT'
      } else if (platform === 'Mobile / Desktop') {
        if (isEmpty(mobileWording) && isEmpty(desktopWording)) {
          emptyFieldDetection = 'WORDING CONTENT UNTUK SALAH SATU PLATFORM MOBILE ATAU DESKTOP'
        }
      }
    }
    return emptyFieldDetection
  }

  function resetFieldAfterChangePlatform (platform, platformIndex) {
    if (platformIndex === 0) {
      setMobileWording('')
      setDesktopWording('')
    } else if (platformIndex === 1) {
      setGlobalWording('')
    }
    setPlatform(platform)
    setSelectedPlatform(platformIndex)
  }

  function handleSubmitContent () {
    const submitErrMsgCheck = errorMsgDetection()
    if (!isEmpty(submitErrMsgCheck)) {
      toast.error(`ANDA BELUM MENGISI FIELD ${submitErrMsgCheck}`, { className: 'font-size-m bold text-center' })
    } else {
      const data =
        {
          company: companyCode,
          location: location,
          platform: platform,
          globalBody: !isEmpty(globalWording) ? globalWording : '',
          desktop: {
            title: title,
            body: !isEmpty(desktopWording) ? desktopWording : ''
          },
          mobile: {
            title: title,
            body: !isEmpty(mobileWording) ? mobileWording : ''
          },
          status: 10
        }
      fetchInsertWordingContent(data, null, null, setSuccess)
      $('#addNewContentWording').modal('hide')
      resetField()
    }
  }

  function resetField () {
    setTitle('')
    setCompanyCode('All')
    setLocation('')
    setMobileWording('')
    setDesktopWording('')
    setGlobalWording('')
    setPlatform('Mobile & Desktop')
    setSelectedPlatform(0)
  }

  function isEnter (e) {
    if (e.keyCode === 13) {
      searchingWordingContent(searchContent)
    }
  }

  return (
    <AppWrapper>
      <div>
        <ToastContainer
          position='top-center'
          autoClose={5500}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          draggable
          style={{ width: '50%' }}
        />
        <header className='top-main-navigation cms-header' id='top-sticky'>
          <div className='container-fluid-cms'>
            <HeaderArea title='Wording Dashboard' />
            <CompanyFilter companyCode={filterCompanyCode} changeCompanyCode={changeCompanyCode} title='Company Code: ' changeFrom='header' all />
            <div className='cms-header-middle header-middle-homepage'>
              <div className='row'>
                <div className='col-xs-8'>
                  <button type='button' className='btn btn-primary btn-m cursor-pointer' data-toggle='modal' data-target='#addNewContentWording'>
                    <i className='anticon icon-plus' /> Create New
                  </button>
                </div>
                <div className='col-xs-4 text-right'>
                  <input
                    type='text'
                    className='input search-bar-cms font-size-s'
                    placeholder='Search by title,location...'
                    onChange={handleInput(setSearchContent)}
                    onKeyUp={isEnter}
                  />
                  <i className='anticon icon-search1' />
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className='container padding-top-m'>
          <div className='row'>
            <div className='col-xs-12'>
              <WordingContentRendezvous
                isLoading={isLoading}
                listWordingContent={listWordingContent}
                getListWordingContent={getListWordingContent}
                syncCurrentOffset={syncCurrentOffset}
                handleInput={handleInput}
              />
            </div>
            <div className='col-xs-12 margin-top-l margin-bottom-xl no-padding-right'>
              <Pagination
                page={page}
                setPage={setPage}
                offset={offset}
                setOffset={setOffset}
                limit={10}
                data={listWordingContent}
                getData={getListWordingContent}
                setSyncCurrentOffset={setSyncCurrentOffset}
              />
            </div>
          </div>
        </div>
        <AddNewContentModal
          handleSubmitContent={handleSubmitContent}
          companyCode={companyCode}
          setCompanyCode={setCompanyCode}
          setPlatform={setPlatform}
          selectedPlatform={selectedPlatform}
          setSelectedPlatform={setSelectedPlatform}
          handleInput={handleInput}
          title={title}
          setTitle={setTitle}
          location={location}
          setLocation={setLocation}
          mobileWording={mobileWording}
          setMobileWording={setMobileWording}
          desktopWording={desktopWording}
          setDesktopWording={setDesktopWording}
          globalWording={globalWording}
          setGlobalWording={setGlobalWording}
          changeCompanyCode={changeCompanyCode}
          resetFieldAfterChangePlatform={resetFieldAfterChangePlatform}
        />
      </div>
    </AppWrapper>
  )
}
