import React, { Fragment } from 'react'
import axios from 'axios'
import _ from 'lodash'

export default class ItemDetail extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      nama: '',
      edit: false
    }
  }

  componentDidMount(){
      const {data}= this.props
      this.setState({
          nama: data.nama
      })
  }

  handleEdit=(e)=>{
    this.setState({
      nama:e.target.value
    })
  }

  handleUpdate = () => {

    const {edit}=this.state

    const newData = {
        nama: this.state.nama,
    }

    if (edit) {
      axios.put(`http://localhost:3001/person/${this.props.data._id}`, newData) 
      .then(res => {
          this.props.handleGetData()
      })
    }
    this.setState({
      edit: !edit
    })
  }


  render () {
    const {handleDelete, data} = this.props
    const {  edit } = this.state
    return (
      <>
        <div style={{ display: 'flex', flexDirection: 'column', marginTop: 50, width: 50 }}>
          <div style={{ display: 'flex' }}>
            <input value={this.state.nama} onChange={(e) => edit && this.handleEdit(e)} />
            <button onClick={() => this.handleUpdate(data)}>
              {!edit ? 'edit':'submit'}
            </button>
          </div>
          <button onClick={() => handleDelete(data)}>delete</button>
        </div>

      </>
    )
  }
}
