import _, { isEmpty } from 'lodash'
import { toast } from 'react-toastify'

const handleErrorMessage = (message, setErr) => {
  if (setErr !== null) {
    setErr({ err: true, message: message })
  }
  toast.error(message, { className: 'font-size-m bold text-center' })
}

const handleSuccessMessage = (message, setSuccess, response) => {
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: BaseRequestGroup.js ~ line 12 ~ response', response)
  if (_.isEmpty(response.data)) {
    setSuccess({ success: true, data: {} })
  } else {
    setSuccess({ success: true, data: response.data })
  }
  if (!_.isEmpty(message)) {
    toast.success(message, { className: 'font-size-m bold text-center' })
  }
}

export const fetchingGetCampaigns = async (setRes, setIsLoading, setErr, api) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)
    if (response.status === 200) {
      if (!isEmpty(response.data && response.data.errors)) {
        setErr({ err: true, message: response.data.errors.message }
        )
      } else {
        // success
        if (setRes !== null) setRes(response.data)
        if (setErr !== null) setErr({ err: false, message: '' })
      }
    } else {
      // const errMsg = `Error response status: ${response.status}, while trying to ${name}`
      // if (setErr !== null) setErr({ err: true, message: errMsg })
      // setErr({ err: true, message: response.data.errors.message })
      let message
      if (response.data) {
        message = response.data.message
      } else {
        message = response.problem
      }
      handleErrorMessage(message, setErr)
    }
  } catch (error) {
    // const errMsg = 'Terjadi kesalahan, ulangi beberapa saat lagi '
    // if (setErr !== null) setErr({ err: true, message: errMsg })
  }
  if (setIsLoading !== null) setIsLoading(false)
}

// fetchingPostPut need more modify
export const fetchingPostPut = async (body, setIsLoading, setErr, setSuccess, api, message, callback, id) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)

    if (response.status === 200) {
      if (response.data && response.data.errors && !isEmpty(response.data.errors)) {
        handleErrorMessage(response.data.errors, setErr)
      } else {
        if (setErr !== null) {
          setErr({ err: false, message: '' })
        }

        if (setSuccess !== null) {
          handleSuccessMessage(message, setSuccess, response)
        }
      }

      if (callback !== null && id !== null) {
        callback(id)
      } else if (callback !== null && id !== null) {
        callback()
      }
    } else {
      message.error = `Error response status: ${response.status}, while trying to ${message.name}`
      // handleErrorMessage(message.error, setErr)
    }
    // }
  } catch (error) {
    // handleErrorMessage(message.error, setErr)
  }
  if (setIsLoading !== null) setIsLoading(false)
}

// getCampaign
export const fetchingDetailCampaign = async (setRes, setIsLoading, setErr, api) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)
    if (response.status === 200) {
      if (!isEmpty(response.data && response.data.errors)) {
        setErr({ err: true, message: response.data.errors.message }
        )
      } else {
        // success

        if (setRes !== null) setRes(response.data)
        if (setErr !== null) setErr({ err: false, message: '' })
      }
    } else {
      handleErrorMessage(response.problem, setErr)
    }
  } catch (error) {
  }
  if (setIsLoading !== null) setIsLoading(false)
}

// getEditoryByType
// getCampaign
export const fetchingEditorByType = async (setRes, setIsLoading, setErr, api) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)
    if (response.status === 200) {
      if (!isEmpty(response.data && response.data.errors)) {
        setErr({ err: true, message: response.data.errors.message }
        )
      } else {
        // success

        if (setRes !== null) setRes(response.data)
        if (setErr !== null) setErr({ err: false, message: '' })
      }
    } else {
      handleErrorMessage(response.problem, setErr)
    }
  } catch (error) {
  }
  if (setIsLoading !== null) setIsLoading(false)
}

export const baseFetchingRequest = async (setIsLoading, setErr, setSuccess, api, message, callback, id) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)

    if (response.status === 200) {
      if (response.data && response.data.errors && !isEmpty(response.data.errors)) {
        handleErrorMessage(response.data.errors, setErr)
      } else {
        if (setErr !== null) {
          setErr({ err: false, message: '' })
        }

        if (setSuccess !== null) {
          handleSuccessMessage(message, setSuccess, response)
        }
      }

      if (callback !== null && id !== null) {
        callback(id)
      } else if (callback !== null && id !== null) {
        callback()
      }
    } else {
      if (response.status === 403) {
        console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: BaseRequestGroup.js ~ line 162 ~ response', response.data)
        // setSuccess([])
        setErr(response.data)
      }
      message.error = `Error response status: ${response.status}, while trying to ${message.name}`
      // handleErrorMessage(message.error, setErr)
    }
    // }
  } catch (error) {
    // handleErrorMessage(message.error, setErr)
  }
  if (setIsLoading !== null) setIsLoading(false)
}

// immprovement post and put
export const fetchingPostPutData = async (setIsLoading, setErr, setSuccess, api, message, setCallBack, id) => {
  if (setIsLoading !== null) setIsLoading(true)
  try {
    const response = await (api)

    if (response.status === 200) {
      if (response.data && response.data.errors && !isEmpty(response.data.errors)) {
        handleErrorMessage(response.data.errors, setErr)
      } else {
        if (setErr !== null) {
          setErr({ err: false, message: '' })
        }

        if (setSuccess !== null) {
          handleSuccessMessage(message, setSuccess, response)
        }
      }

      if (setCallBack !== null && id !== null) {
        setCallBack(true)
      }
    } else {
      message.error = `Error response status: ${response.status}, while trying to ${message.name}`
      // handleErrorMessage(message.error, setErr)
    }
    // }
  } catch (error) {
    // handleErrorMessage(message.error, setErr)
  }
  if (setIsLoading !== null) setIsLoading(false)
}
