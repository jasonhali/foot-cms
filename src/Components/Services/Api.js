// import config from '../../../config'
// import _ from 'lodash'
// a library to wrap and simplify api calls
import apisauce from 'apisauce'
// import config from '../config'

const apiURL = 'https://jsonplaceholder.typicode.com'

const feetFootUrl = 'http://localhost:3001/'

const create = (baseURL = apiURL) => {
  const feetFootWrapper = apisauce.create({
    baseURL: feetFootUrl,
    timeout: 20000
  })

  const headers = {
    'Content-Type': 'application/json'
  }

  const getAllCampaigns = (data) => {
    return feetFootWrapper.get('/', {}, { headers: { ...headers } })
  }

  const getActiveCampaignUrl = (data) => {
    console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: Api.js ~ line 26 ~ data', data)
    return feetFootWrapper.get(`/campaign/search?keyword=${data}`, {}, { headers: { ...headers } })
  }

  const getActiveUrl = (data) => {
    return feetFootWrapper.get('/url', { data }, { headers: { ...headers } })
  }

  const createCampaigns = (data) => {
    return feetFootWrapper.post('/campaigns', { ...data }, { headers: { ...headers } })
  }

  const deleteComponent = (id) => {
    return feetFootWrapper.delete(`/campaigns/${id}`, {}, { headers: { ...headers } })
  }

  const updateComponent = (id, data) => {
    return feetFootWrapper.put(`/campaigns/${id}`, { ...data }, { headers: { ...headers } })
  }

  // items
  const getAllItems = (data) => {
    return feetFootWrapper.get('/item', {}, { headers: { ...headers } })
  }

  const deleteItems = (id) => {
    return feetFootWrapper.delete(`/item/${id}`, {}, { headers: { ...headers } })
  }

  // item by sku
  const getItemsByValue = (sku) => {
    return feetFootWrapper.get(`/item?sku=${sku}`, {}, { headers: { ...headers } })
  }

  const addNewItems = (data) => {
    return feetFootWrapper.post('/item', { ...data }, { headers: { ...headers } })
  }

  const updateItems = (id, data) => {
    return feetFootWrapper.put(`/item/${id}`, { ...data }, { headers: { ...headers } })
  }

  // detail
  const getEditorTool = (data) => {
    return feetFootWrapper.get('/editorTools', {}, { headers: { ...headers } })
  }

  const getEditorToolByType = (type) => {
    return feetFootWrapper.get(`/editor/${type}`, {}, { headers: { ...headers } })
  }

  const getDetailCampaign = (id) => {
    return feetFootWrapper.get(`/campaigns/${id}`, {}, { headers: { ...headers } })
  }

  // content
  const getAllContents = (data) => {
    return feetFootWrapper.get('/contents', {}, { headers: { ...headers } })
  }

  const getAllContentsByType = (type) => {
    return feetFootWrapper.get(`/contents/data?type=${type}`, {}, { headers: { ...headers } })
  }

  const postContentsByType = (data) => {
    return feetFootWrapper.post('/contents/data', { data }, { headers: { ...headers } })
  }

  const putContentsByType = (id, data) => {
    return feetFootWrapper.put(`/contents/data/${id}`, { data }, { headers: { ...headers } })
  }

  const deleteContentsByType = (id) => {
    return feetFootWrapper.delete(`/contents/data/${id}`, {}, { headers: { ...headers } })
  }

  // user
  const getuserData = (payload) => {
    return feetFootWrapper.post('/login', { ...payload }, { headers: { ...headers } })
  }

  const getUserByAuth = (jwt) => {
    const headersData = {
      ...headers,
      Authorization: jwt
    }
    console.log('trace 4 \nheader =', headersData)
    feetFootWrapper.setHeaders(headersData)
    return feetFootWrapper.get('/login')
  }

  return {
    // me
    getUserByAuth,
    getuserData, // user datas
    getAllCampaigns,
    getActiveCampaignUrl, // search campaign
    createCampaigns,
    deleteComponent,
    updateComponent,
    getActiveUrl,
    // items
    getAllItems,
    deleteItems,
    addNewItems,
    updateItems,
    getItemsByValue, // bysku
    // detailCampaign
    getEditorTool,
    getDetailCampaign,
    // editor
    getEditorToolByType,
    // content
    getAllContents,
    // byType
    getAllContentsByType,
    postContentsByType,
    putContentsByType,
    deleteContentsByType
  }
}

// let's return back our create method as the default.
export default {
  create
}
