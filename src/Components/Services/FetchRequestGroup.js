import { baseFetchingRequest, fetchingDetailCampaign, fetchingEditorByType, fetchingGetCampaigns, fetchingPostPut, fetchingPostPutData } from './BaseRequestGroup'
import API from './Api'

const api = API.create()

const fetchGetAllCampaigns = (setRes, setIsLoading, setErr) => {
  fetchingGetCampaigns(setRes, setIsLoading, setErr, api.getAllCampaigns())
}

const fetchGetCampaignsByName = (setRes, setIsLoading, setErr, param) => {
  fetchingGetCampaigns(setRes, setIsLoading, setErr, api.getActiveCampaignUrl(param))
}

const fetchGetActiveCampaignUrl = (setRes, setIsLoading, setErr, value) => {
  fetchingGetCampaigns(setRes, setIsLoading, setErr, api.getActiveUrl(value))
}

// nama funct sesuai dari api.js
const addNewCampaigns = (body, setIsLoading, setErr, setSuccess, message) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.createCampaigns(body), message)
}

const deleteCampaignsData = (body, setIsLoading, setErr, setSuccess, message) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.deleteComponent(body), message)
}

const updateCampaignsData = (body, setIsLoading, setErr, setSuccess, message, id, callback) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.updateComponent(id, body), message, callback, id)
}

// items
const fetchGetAllItems = (setRes, setIsLoading, setErr) => {
  fetchingGetCampaigns(setRes, setIsLoading, setErr, api.getAllItems())
}

const deleteItemData = (body, setIsLoading, setErr, setSuccess, message, callback) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.deleteItems(body), message, callback)
}

const fetchItemsByValue = (param, setRes, setIsLoading, setErr) => {
  fetchingGetCampaigns(setRes, setIsLoading, setErr, api.getItemsByValue(param))
}

const addNewItems = (body, setIsLoading, setErr, setSuccess, message, callback) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.addNewItems(body), message, callback)
}

const updateItems = (body, setIsLoading, setErr, setSuccess, message, id, callback) => {
  fetchingPostPut(body, setIsLoading, setErr, setSuccess, api.updateItems(id, body), message, callback)
}

// detail
const getEditorTool = (setRes, setIsLoading, setErr) => {
  fetchingDetailCampaign(setRes, setIsLoading, setErr, api.getEditorTool())
}

const getDetailCampaign = (setRes, setIsLoading, setErr, campaignId) => {
  fetchingDetailCampaign(setRes, setIsLoading, setErr, api.getDetailCampaign(campaignId))
}

// editorType
const getEditorToolByType = (setRes, setIsLoading, setErr, type) => {
  fetchingEditorByType(setRes, setIsLoading, setErr, api.getEditorToolByType(type))
}

// content
const fetchGetAllContent = (setRes, setIsLoading, setErr) => {
  fetchingDetailCampaign(setRes, setIsLoading, setErr, api.getAllContents())
}

// get content data
const getAllContentData = (setIsLoading, setIsErr, setContentData, message, apiParam) => {
  baseFetchingRequest(setIsLoading, setIsErr, setContentData, api.getAllContentsByType(apiParam), message)
}

// get user data

// get user data
const getUserDataJWT = (setIsLoading, setIsErr, setSuccess, message, apiParam, setCallBack) => {
  fetchingPostPutData(setIsLoading, setIsErr, setSuccess, api.getuserData(apiParam), message, setCallBack)
}

const getUserData = (setIsLoading, setIsErr, setSuccess, message, apiParam, setCallBack) => {
  baseFetchingRequest(setIsLoading, setIsErr, setSuccess, api.getUserByAuth(apiParam), message, setCallBack)
}

// check is active user data
const getActiveUserData = (setIsLoading, setIsErr, setContentData, message, apiParam) => {
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: FetchRequestGroup.js ~ line 109 ~ apiParam', apiParam)
  baseFetchingRequest(setIsLoading, setIsErr, setContentData, api.getIsActiveUser(apiParam), message)
}

// post content data
const postContentData = (setIsLoading, setIsErr, setContentData, message, apiParam, setCallBack) => {
  fetchingPostPutData(setIsLoading, setIsErr, setContentData, api.postContentsByType(apiParam), message, setCallBack)
}

// put content data
const postPutContentData = (setIsLoading, setIsErr, setSuccess, contentData, message, apiParam, setCallBack) => {
  fetchingPostPutData(setIsLoading, setIsErr, setSuccess, api.putContentsByType(apiParam, contentData), message, setCallBack)
}

// delete content data
const postDeleteContentData = (setIsLoading, setIsErr, setSuccess, message, apiParam, setCallBack) => {
  fetchingPostPutData(setIsLoading, setIsErr, setSuccess, api.deleteContentsByType(apiParam), message, setCallBack)
}

export {
  getActiveUserData,
  getUserData, // user
  getUserDataJWT,
  fetchGetAllCampaigns,
  fetchGetCampaignsByName,
  addNewCampaigns,
  deleteCampaignsData,
  updateCampaignsData,
  fetchGetActiveCampaignUrl,
  fetchGetAllItems,
  fetchItemsByValue, // item by sku
  // items
  addNewItems, // post
  updateItems, // put
  deleteItemData,
  // detail
  getEditorTool,
  getDetailCampaign,
  getEditorToolByType, // editor by type
  // content
  fetchGetAllContent,
  // content byType
  getAllContentData,
  postContentData,
  postPutContentData,
  postDeleteContentData
}
