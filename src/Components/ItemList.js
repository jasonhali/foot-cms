import React, { Fragment } from 'react'
import axios from 'axios'
import _ from 'lodash'
import ItemDetail from './ItemDetail'

export default class ItemList extends React.Component {
  constructor (props) {
    super(props)
    // Don't call this.setState() here!
    this.state = {
       persons: [],
       fetch: true
    }
  }

  componentDidMount () {
    axios.get('http://localhost:3001/')
      .then(res => {
        const persons = res.data
        this.setState({ persons })
    })
  }


  handleDelete = (data) => {
    // axios.delete(`http://localhost:3001/person/${data._id}`) 
    // .then(res => {
    //   this.handleGetData()
    // })
  }

  handleGetData =()=>{
    axios.get('http://localhost:3001/')
    .then(res => {
      const persons = res.data
      this.setState({ persons })
    })
  }

  handlePost = () => {
    const newPerson = {
        nama: 'user tes',
        umur: 21
    }

    axios.post(`http://localhost:3001/person`, newPerson) 
    .then(res => {
        const person = res.data
        this.setState({ persons: [...this.state.persons,person] })
      })
  }

 
  render () {
    const { persons } = this.state
    return (
      <>
        <button onClick={()=>this.handlePost()}>ADD ITEM</button>

        {
          !_.isEmpty(persons) && 
          persons.map((data, index) => (
            <Fragment>
              <ItemDetail
                handleDelete={this.handleDelete}
                data={data}
                handleUpdate={this.handleUpdate}
                handleGetData={this.handleGetData}
              />
            </Fragment>
          ))
        }

        
      </>
    )
  }
}
