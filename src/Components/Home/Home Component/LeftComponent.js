import React from 'react'
import _ from 'lodash'
import AddRounded from '@mui/icons-material/AddRounded'
import AddToDrive from '@mui/icons-material/AddToDrive'
import PostAdd from '@mui/icons-material/PostAdd'
import { Link } from 'react-router-dom'
import SourceIcon from '@mui/icons-material/Source'
import InventoryIcon from '@mui/icons-material/Inventory'
import config from '../../../config'
export default class LeftComponent extends React.Component {
  constructor (props) {
    super(props)
    const { userData } = this.props.context
    this.state = {
      dataOption: [
        { name: 'Microsite', icon: <AddToDrive />, dir: '', location: config.navigationUrl + '/' },
        { name: 'Manage Items', icon: <PostAdd />, dir: 'item', location: config.navigationUrl + '/item' },
        { name: 'Content', icon: <SourceIcon />, dir: 'content', location: config.navigationUrl + '/content' }
      ],
      active: 0,
      activeUrl: ''
    }
  }

  handleNavigation (idx, activeUrl) {
    const { handleChangeUrl } = this.props.context
    handleChangeUrl(activeUrl)
    this.setState({ active: idx })
  }

  render () {
    const { triggerModal, config } = this.props.context

    let activeUrl = { ...config }
    if (!_.isEmpty(config)) {
      activeUrl = activeUrl.baseUrl
    }

    return (
      <div className='flex h-full w-full column align-item-c'>
        <div id='CreateNew' className='flex center m-top-half w-ex-full' style={{ color: 'black' }} onClick={() => triggerModal()}>
          <AddRounded fontSize='medium' />
          <p> Create New </p>
        </div>
        {
            this.state.dataOption.map(
              (data, idx) => {
                if (idx === 1) {
                  activeUrl = activeUrl + 'item'
                }
                // activeDir = activeDir + data.dir
                return (
                  <Link to={`/${data.dir}`} key={idx} className='w-full h-half' id={`home-navigation-${idx + 1}`}>
                    <div className={`flex w-full center ${window.location.href === data.location ? 'active' : ''}`} onClick={() => this.handleNavigation(idx, activeUrl)} id='MainOption'>
                      <div className='w-ex-half flex end'>
                        {data.icon}
                      </div>
                      <div className='w-medium-ex-half flex start p-left-ex-tiny'>
                        {data.name}
                      </div>
                    </div>
                  </Link>
                )
              }
            )
        }

      </div>
    )
  }
}
