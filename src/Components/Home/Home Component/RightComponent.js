import React, { useContext, useState } from 'react'
import _ from 'lodash'
import moment from 'moment'
import { StateContext } from '../../StateContext'
import { Link } from 'react-router-dom'
import { height, style } from '@mui/system'
import CustomLazyLoadImage from '../Campaigns/functions/CustomlazyLoadImage'

export default function BodyRightComponent (props) {
  const [context] = useContext(StateContext)
  const { type } = props

  function RenderCampaign () {
    const { campaigns, onHandleDeleteCampaign, editCampaigns } = context
    const altImg = 'https://drive.google.com/uc?export=view&id=1fDDr6mpIpH7D7HgmL2Tj56Vh3cd_OUfl'

    const [loadingImage, isLoadingImage] = useState(false)

    return (
      !_.isEmpty(campaigns)
        ? <div id='rc-campaign-cnt'>
          {campaigns.map((data, idx) => {
            return (
              <div key={idx} id='campaign-item' className={`shadow column relative ${idx > 3 ? 'm-top' : ''}`}>

                <div className='w-tiny h-full absolute' id='left-cnt'>
                  <div id='left' className='w-full h-full absolute' onClick={(e) => editCampaigns(data._id, data)} />
                </div>
                <div className='w-tiny h-full absolute' id='right-cnt'>
                  <div id='right' className='w-full h-full absolute' onClick={() => onHandleDeleteCampaign(data._id)} />
                </div>

                <Link to={`/campaigns/${data._id}`} style={{ minHeight: '10em', maxHeight: '10em' }}>
                  <div className='rc-container-redirect h-full'>
                    <div id='rc-campaign-img' style={{ height: '78%', background: '#252627', borderTopLeftRadius: 4, borderTopRigghRadius: 4 }} className='h-full w-full center a-end'>
                      <CustomLazyLoadImage
                        loadingImage={loadingImage}
                        isLoadingImage={isLoadingImage}
                        prevIsLoading
                        image='https://res.cloudinary.com/donu8hbzs/image/upload/v1656770446/Content%20Management%20System/Brand%20Logo/FeetFoot/FeetFoot_aat2bx.png'
                      />
                      {/* <img src='' alt={altImg} className='rc-campaign-img' /> */}
                    </div>
                    <div id='rc-campaign-detail' className='w-full center column'>
                      <div id='rc-campaign-detail-1' className='h-full w-full center'>
                        <p>{data.campaignName}</p>
                      </div>
                      <div id='rc-campaign-detail-2' className='h-full w-full'>
                        <p>Start at {moment(data.startDate).format('dddd Do YYYY')}
                        </p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            )
          })}
          </div>
        : <h1>Campaign tidak ditemukan</h1>
    )
  }

  function renderItems () {
    const { items, editItems, triggerFromBody, fromBody, onHandleDeleteCampaign } = context
    // getAllItems()
    // const altImg = 'https://drive.google.com/uc?export=view&id=1fDDr6mpIpH7D7HgmL2Tj56Vh3cd_OUfl'
    const ItemOption = [
      { name: 'No', value: 'No' },
      { name: 'Item Name', value: 'itemName' },
      { name: 'SKU', value: 'sku' },
      { name: 'Rating', value: 'itemRating' },
      { name: 'Size Qty', value: 'itemSize' },
      { name: 'Retail Price', value: 'retailPrice' },
      { name: 'Colorway', value: 'colorway' },
      { name: 'Gender', value: 'gender' },
      { name: 'Type', value: 'itemType' },
      { name: 'Brand', value: 'brand' },
      { name: 'Release Date', value: 'releaseDate' }
    ]

    return (
      <div id='rc-campaign-cnt column' className='h-inherit w-inherit'>
        <div
          className='h-tiny w-full center'
        >
          <div
            className='h-full w-inherit flex justify-se' style={{
              width: '95%',
              borderBottom: '4px solid #888888'
            }}
          >
            {!_.isEmpty(ItemOption) &&
            ItemOption.map((optionData, idx) => (
              <div key={idx} id='itemHeader' className='endPosition w-full' style={{ paddingBottom: '0.5%', fontWeight: 'bold' }}>
                {optionData.name}
              </div>
            ))}
          </div>
        </div>

        <div
          className='h-full w-full' style={{
            maxHeight: '90%',
            textAlign: '-webkit-center'
          }}
        >
          <div
            className='w-inherit flex h-full' id='item-RC'
          >
            {!_.isEmpty(items) &&
            items.map((itemData, idx) => {
              return (
                <div key={idx} className='center w-full primary-space-btm' onClick={() => triggerFromBody(!fromBody)}>
                  <div id='editor' style={{ position: 'absolute', display: 'none', width: '80%', justifyContent: 'center' }}>
                    <div id='itemEditor' className='m-right-ex-small center bold' onClick={() => editItems(itemData._id, itemData)}>Edit</div>
                    <div id='itemDeletor' className='center bold' onClick={() => onHandleDeleteCampaign(itemData._id, 'item')}>Delete</div>
                  </div>

                  <div className='container item-list'>
                    {ItemOption.map((dataOption, id) => (
                      <div
                        id='itemBody' className={`w-full item-${idx}`} key={id}
                      >
                        {id === 0 ? idx + 1 : id === 4 ? itemData[dataOption.value].length : itemData[dataOption.value]}
                      </div>
                    ))}
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }

  function renderContent () {
    const { items, editItems, triggerFromBody, fromBody, content } = context

    return (
      <div id='rc-content-cnt' className='h-inherit w-inherit'>
        {!_.isEmpty(content) &&
        content.map((data, idx) => (
          <Link to={`/content/${data._id}?type=${data.data.type}`} key={idx} className='rc-content-redirect h-full'>
            <div id='rc-campaign-content-img' className='h-full w-full center a-end'>

              <CustomLazyLoadImage
                image={data.data?.img_preview}
                className='collection'
              />
              {/* <img src='https://drive.google.com/uc?export=view&id=1fDDr6mpIpH7D7HgmL2Tj56Vh3cd_OUfl' className='rc-campaign-img' /> */}
            </div>
            <div id='rc-campaign-content-detail' className='w-full center column'>
              <p>{data.data.title}</p>
            </div>
          </Link>
        ))}
      </div>
    )
  }

  return (
    <div className='center h-full w-full column'>
      {type !== 'item' && type !== 'content' &&
        <div className='w-full flex main-tab-right h-ex-small a-end'>
          <p className='bold p-left-ex-tiny m-unset f-ex-large'>
            Display All Microsite
          </p>
        </div>}
      <div className='w-full main-tab-right h-full center flex'>
        {type === 'item'
          ? renderItems()
          : type === 'content'
            ? renderContent()
            : RenderCampaign()}
      </div>
    </div>
  )
}
