import React, { Fragment, useContext, useEffect, useState } from 'react'
// import logo from './logo.svg'
// import { Routes, Route, BrowserRouter } from 'react-router-dom'
import 'react-day-picker/lib/style.css'
// import 'react-day-picker/lib/style.css'
// import { toast, ToastContainer } from 'react-toastify'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { StateContext, StateProvider } from '../../StateContext'
import CampaignDisplayBody from './styles/functions/Modules/DisplayBody/CampaignDisplayBody'
import CampaignDisplayHeader from './styles/functions/Modules/DisplayBody/CampaignDisplayHeader'

// import _ from 'lodash'

export default function CampaignDisplay (props) {
  const [context] = useContext(StateContext)
  const { detailCampaign, isLoading } = context

  useEffect(() => {
  }, [])

  return (
    <>
      <CampaignDisplayHeader detailCampaign={detailCampaign} />
      <CampaignDisplayBody detailCampaign={detailCampaign} isLoading={isLoading} />
    </>
  )
}
