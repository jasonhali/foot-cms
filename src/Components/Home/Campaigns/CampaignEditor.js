import _ from 'lodash'
import React, { Fragment, useContext, useEffect, useState } from 'react'
import 'react-day-picker/lib/style.css'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { getEditorToolByType } from '../../Services/FetchRequestGroup'
import { StateContext } from '../../StateContext'

// import _ from 'lodash'

export default function CampaignEditor (props) {
  const [context] = useContext(StateContext)
  const { detailEditorTools, editorType, triggerExpandEditor, setEditorType, expandEditor, setDetailEditorToolByType, setIsLoading } = context

  useEffect(() => { // get editor by type
    if (expandEditor) {
      getEditorToolByType(setDetailEditorToolByType, setIsLoading, null, editorType)
    }
  }, [editorType, expandEditor])

  const handleChangeEditorType = (editorName) => {
    setEditorType(() => editorName, triggerExpandEditor(// callback
      (!(((editorName === editorType) && expandEditor))),
      setDetailEditorToolByType([])
    ))
  }

  return (
    <>
      <div className='campaign-editor'>
        {detailEditorTools?.map((editorData, idx) => {
          editorData = editorData.data

          return (
            <div className={`campaign-editor-data ${editorData.name === editorType && expandEditor ? 'active' : ''} type-${idx}`} key={idx} onClick={(e) => handleChangeEditorType(editorData.name)}>
              <img className='campaign-editor-data-img' src={editorData.image_url} />
              <div className='campaign-editor-data-title'>
                {editorData.name}
              </div>
            </div>
          )
        })}
      </div>
    </>
  )
}
