import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew'
import CloudDoneOutlinedIcon from '@mui/icons-material/CloudDoneOutlined'

import Input from './functions/Input'
import Button from './functions/Button'
import { Link } from 'react-router-dom'
import { AddRounded } from '@mui/icons-material'
export default function CampaignHeader ({ isContent, handleCreateNewContent, userData, navigateTo }) {
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: CampaignHeader.js ~ line 9 ~ userData', userData)
  return (
    <div className='campaign-header'>
      <Link to='/' className='campaign-link'>
        <div className='campaign-header-icon'>
          <ArrowBackIosNewIcon sx={{ fontSize: 16 }} />
        </div>
        <div className='campaign-header-text' onClick={() => navigateTo('campaign')}>
          Beranda
        </div>
      </Link>

      <div className='campaign-header-title'>
        {!isContent
          ? <Input value='adsaadsaadsaadsa adsaadsaadsaadsa' type='transparent' name='campaign-header' height='unset' placeholder='Design Tanpa Judul' />
          : <Input
              value='TOP 10 PRODUCT' type='transparent' isReadOnly name='campaign-header' height='unset'
            />}
      </div>

      <div className='campaign-header-account'>
        <div className='campaign-header-account-border'>
          <div>
            JH
          </div>
        </div>
      </div>

      <div className='campaign-header-backup'>
        {!isContent
          ? <Button value='Backup' type='secondary' size='l' fontSize='s' name='campaign-header' fontWeight='medium' height='unset' icon={<CloudDoneOutlinedIcon className='padding-right-xs' sx={{ fontSize: 30 }} />} />
          : <Button
              value='Buat Desain'
              type='secondary'
              size='l'
              fontSize='s'
              name='campaign-header'
              fontWeight='medium'
              height='unset'
              handleOnClick={handleCreateNewContent}
              icon={
                <AddRounded
                  className='padding-right-xs'
                  sx={{ fontSize: 25 }}
                />
              }
            />}
      </div>
    </div>
  )
}
