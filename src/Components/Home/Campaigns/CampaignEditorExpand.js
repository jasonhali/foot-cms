import React, { Fragment, useContext, useEffect, useState } from 'react'
// import logo from './logo.svg'
// import { Routes, Route, BrowserRouter } from 'react-router-dom'
import 'react-day-picker/lib/style.css'
// import 'react-day-picker/lib/style.css'
// import { toast, ToastContainer } from 'react-toastify'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { StateContext } from '../../StateContext'
// import { StateContext, StateProvider } from '../../StateContext'
import AddIcon from '@mui/icons-material/Add'

import _ from 'lodash'
import config from '../../../config'
import Button from './functions/Button'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

import CustomLazyLoadImage from './functions/CustomlazyLoadImage'
import { getDetailCampaign, updateCampaignsData } from '../../Services/FetchRequestGroup'

export default function CampaignEditorExpand (props) {
  const [context] = useContext(StateContext)
  const { expandEditor, triggerExpandEditor, editorType, detailEditorToolByType, detailCampaign, setSuccess, setDetailCampaign, setIsLoading, callbackDetailCampaign, handleUpdateCampaign } = context

  const [editorData, setEditorData] = useState([])

  const [loadingImage, isLoadingImage] = useState(false)

  useEffect(() => {
    if (!_.isEmpty(detailEditorToolByType)) {
      setEditorData(detailEditorToolByType)
    } else {
      setEditorData(null)
    }
  }, [detailEditorToolByType])

  function renderEditorImage (dataType, idx) {
    if (dataType.image_preview) {
      return (
        <CustomLazyLoadImage
          loadingImage={loadingImage}
          isLoadingImage={isLoadingImage}
          prevIsLoading
          image={config.imageUrl + dataType.image_preview}
          className={idx > 0 ? 'margin-top-s' : ''}
        />
      )
    } else {
      return (
        <div className={`divider ${idx > 0 ? 'margin-top-l' : ''} divider-${idx}`}>
          <Skeleton height={idx + 1 <= 2 ? (idx + 1) * 15 : (idx + 1) * 10} width='80%' enableAnimation={false} className='divider-skeleton' />
        </div>
      )
    }
  }

  const renderEditorToolsComponent = () => {
    return (
      editorData.map((dataType, idx) => {
        dataType = dataType.data
        return (
          <div className={`renderTool ${dataType.type.toLowerCase()}-component`} key={idx}>
            {renderEditorImage(dataType, idx)}

            <div className={`actionHover part-${idx}`}>
              <div className='actionHover-background' />
              <div className='actionHover-action'>
                {!loadingImage &&
                  <>
                    <p className='padding-top-xxl margin-top-xxl'>{dataType.name.toUpperCase()}</p>
                    <Button
                      value='Add'
                      type='secondary'
                      additionalClass='margin-top-xs'
                      iconAdditionalClass='margin-top-xs'
                      size='s'
                      fontSize='s'
                      name='campaign-header'
                      fontWeight='bold'
                      height='unset'
                      width=''
                      handleOnClick={() => handleUpdateCampaign(dataType, `Add ${dataType.name}`)}
                      icon={<AddIcon className='tes123' sx={{ fontSize: 20 }} />}
                    />
                  </>}
              </div>
            </div>
          </div>
        )
      })
    )
  }

  return (
    <>
      {!_.isEmpty(editorData) &&
        <div className='detail-item-body-expand-container'>
          {renderEditorToolsComponent()}
        </div>}
    </>
  )
}
