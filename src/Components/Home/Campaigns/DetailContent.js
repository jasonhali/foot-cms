import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import 'react-day-picker/lib/style.css'
import 'react-toastify/dist/ReactToastify.css'
import { getAllContentData, postContentData, postDeleteContentData, postPutContentData } from '../../Services/FetchRequestGroup'
import { StateProvider } from '../../StateContext'
import DeleteIcon from '@mui/icons-material/Delete'
import NoteIcon from '@mui/icons-material/Note'
import CampaignHeader from './CampaignHeader'
import BrokenImageIcon from '@mui/icons-material/BrokenImage'

import {
  BrowserRouter as Router,
  useLocation
} from 'react-router-dom'
import { baseFetchingRequest } from '../../Services/BaseRequestGroup'
import ModalContent from '../../Modal/ModalContent'
import CustomLazyLoadImage from './functions/CustomlazyLoadImage'

// import _ from 'lodash'

export default function DetailContent ({ userData, navigateTo }) {
  const [expandEditor, triggerExpandEditor] = useState(false)
  const [addContentQuery, setAddContentQuery] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [isErr, setIsErr] = useState(false)
  const [success, setSuccess] = useState()

  const [contentData, setContentData] = useState(null)

  // modal
  const [toggleModal, setToggleModal] = useState(false)
  const [callBackAllData, setCallBackAllData] = useState(false)
  // check isFromDisplay
  const [isFromDisplay, setIsFromDisplay] = useState(false)
  const [contentDetailID, setContentDetailID] = useState(null)

  function useQuery () {
    const { search } = useLocation()

    return React.useMemo(() => new URLSearchParams(search), [search])
  }

  const query = useQuery().get('type')

  useEffect(() => {
    let data
    if (query === 'TopProduct') {
      data = {
        type: 'TopProduct',
        urlKey: '',
        title: 'The Hot 10',
        product: [
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {}
        ]
      }
    } else {
      // "data": {
      data = {
        type: 'Magazine',
        urlKey: '',
        title: '',
        description: '',
        product: [
          {
            image_url: ''
          },
          {
            type: 'increment'
          }
        ]
      }
    }
    setAddContentQuery(data)
  }, [query])

  useEffect(() => {
    const message = `${query} berhasil dimuat.`

    getAllContentData(setIsLoading, setIsErr, setContentData, message, query)
    setCallBackAllData(() => false,
      toggleModal && setToggleModal(false),
      setContentDetailID(null)
    )
    // callback at the end
  }, [callBackAllData])

  const handleToogleModal = (fromDisplay, data) => {
    if (fromDisplay) {
      setAddContentQuery(() => data.data,
        setContentDetailID(() => data._id,
          setIsFromDisplay(true)
        )
      )
    } else {
      setIsFromDisplay(false)
    }
    setToggleModal(!toggleModal)
  }

  const onSubmitTrigger = () => {
    const message = `${query} berhasil di ${isFromDisplay ? 'Update' : 'Submit'}.`

    if (isFromDisplay) {
      postPutContentData(setIsLoading, setIsErr, setSuccess, addContentQuery, message, contentDetailID, setCallBackAllData)
    } else {
      postContentData(setIsLoading, setIsErr, null, message, addContentQuery, setCallBackAllData)
    }
  }

  function handleCreateNewContent () {
    handleToogleModal()
  }

  function renderTopProduct ({ data, idx }) {
    return (
      <div className='content-data-wrapper' onClick={() => handleToogleModal(true, data)}>
        <div className='topProduct-title center'>
          <h2>
            {data.data.title}
          </h2>
        </div>
        <div className='topProduct-urlKey center'>
          <span>
            Product Url Key
          </span>
          <h3>
            {_.isEmpty(data.data.urlKey) ? 'Data Empty' : data.data.urlKey}
          </h3>
        </div>
        <div className='topProduct-sku'>
          <span>
            Product Sku
          </span>
          <ol>
            {
              data.data.product.map((data, idxData) => (
                <h4 key={idxData}>
                  <li>{_.isEmpty(data) ? 'Data Empty' : data.sku}</li>
                </h4>
              ))
            }
          </ol>
        </div>
        <div className='topProduct-ID center'>
          <h5>
            PRODUCT ID
          </h5>
          <span>
            {data._id}
          </span>
        </div>
      </div>
    )
  }

  const [loadingImage, isLoadingImage] = useState(false)

  function renderMagazine ({ data, idx }) {
    return (
      <div className='content-data-wrapper' onClick={() => handleToogleModal(true, data)}>

        <div className='magazine-urlKey center'>
          {data.data.product[0].image_url !== '' &&
            <CustomLazyLoadImage
              loadingImage={loadingImage}
              isLoadingImage={isLoadingImage}
              prevIsLoading
              image={data.data.product[0].image_url}
              className={idx > 0 ? 'margin-top-s' : ''}
            />}

          {data.data.product[0].image_url === '' &&
            <BrokenImageIcon sx={{ color: '#FEFEFC', fontSize: '9.5em' }} />}
        </div>

        <div className='magazine-bottom center'>
          <NoteIcon sx={{ color: '#EBEBF0', fontSize: 30 }} />
          <span className='margin-left-s'>{`${data?.data?.product?.length - 1} Pages`}</span>
        </div>

      </div>
    )
  }

  function onDeleteContent (data) {
    const message = `${query} berhasil di hapus.`

    postDeleteContentData(setIsLoading, setIsErr, setSuccess, message, data._id, setCallBackAllData)
  }

  function renderContentBody () {
    return (
      <div className='detail-item-body-right detail-item-body-right-content center'>
        {
            contentData.data.map((data, idx) => (
              <div className={`content-data-${query} ${idx > 0 ? 'margin-left-l' : ''}`} key={idx}>
                <div className={`content-data-${query}-delete`} onClick={() => onDeleteContent(data)}>
                  <DeleteIcon sx={{ color: '#FF0000', fontSize: 30 }} />
                </div>
                {query === 'TopProduct' && renderTopProduct({ data, idx })}
                {query === 'Magazine' && renderMagazine({ data, idx })}
              </div>
            ))
          }
      </div>
    )
  }

  return (

    <StateProvider
      addContentQuery={addContentQuery}
      setAddContentQuery={setAddContentQuery}
      query={query}
      isFromDisplay={isFromDisplay}
    >

      <div className={`detail-item content-section-${query}`}>
        <ModalContent toggleModal={toggleModal} handleToogleModal={handleToogleModal} contentData={contentData?.data} query={query} onSubmitTrigger={onSubmitTrigger} isFromDisplay={isFromDisplay} />
        <div className='detail-item-header'>
          <CampaignHeader navigateTo={navigateTo} isContent handleCreateNewContent={handleCreateNewContent} userData={userData} />
        </div>

        <div className={`detail-item-body ${isLoading ? 'center' : ''}`}>
          {isLoading && <div class='lds-roller'><div /><div /><div /><div /><div /><div /><div /><div /></div>}
          {!isLoading && !_.isEmpty(contentData) && renderContentBody()}
        </div>
      </div>

    </StateProvider>
  )
}
