import React, { Fragment, useContext, useEffect, useState } from 'react'
// import logo from './logo.svg'
import _ from 'lodash'
// import { Routes, Route, BrowserRouter } from 'react-router-dom'
import 'react-day-picker/lib/style.css'
// import 'react-day-picker/lib/style.css'
// import { toast, ToastContainer } from 'react-toastify'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { getDetailCampaign, getEditorTool, getEditorToolByType, updateCampaignsData } from '../../Services/FetchRequestGroup'
import { StateProvider } from '../../StateContext'
import CampaignDisplay from './CampaignDisplay'
import CampaignEditor from './CampaignEditor'
import CampaignEditorExpand from './CampaignEditorExpand'
import CampaignHeader from './CampaignHeader'

// import _ from 'lodash'

export default function DetailCampaign ({ userData, navigateTo }) {
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: DetailCampaign.js ~ line 20 ~ userData', userData)
  const [expandEditor, triggerExpandEditor] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [success, setSuccess] = useState({ success: false, data: {} })

  // api
  const [detailEditorTools, setDetailEditorTools] = useState()
  const [detailEditorToolByType, setDetailEditorToolByType] = useState([])

  // activePreviewIndex
  const [activePreviewIndex, setActivePreviewIndex] = useState(0)

  const [detailCampaign, setDetailCampaign] = useState()

  useEffect(() => { // get detail for left content
    const campaignId = window.location.pathname.substring(11, window.location.pathname.length)
    callbackDetailCampaign(campaignId)

    getEditorTool(setDetailEditorTools, setIsLoading, null)
  }, [])

  useEffect(() => { // get callback
  }, [detailCampaign])

  const callbackDetailCampaign = (id, type) => {
    getDetailCampaign(setDetailCampaign, setIsLoading, null, id)
  }

  const [editorType, setEditorType] = useState('')

  const handleUpdateCampaign = (dataType, message, editorDataType) => {
    const spreadCampaign = { ...detailCampaign }
    let spreadCampaignData
    if (editorDataType === 'product') {
      spreadCampaignData = [...spreadCampaign.campaignData]
      spreadCampaignData[activePreviewIndex] = dataType
    } else if (editorDataType === 'image' || editorDataType === 'video') {
      spreadCampaignData = [...spreadCampaign.campaignData]
      spreadCampaignData[activePreviewIndex] = dataType
    } else if (editorDataType === 'text' || editorDataType === 'product-collaboration' || editorDataType === 'celebrity-collection') {
      spreadCampaignData = [...spreadCampaign.campaignData]
      spreadCampaignData[activePreviewIndex] = dataType
    } else {
      spreadCampaignData = [...spreadCampaign.campaignData, { ...dataType }]
    }
    spreadCampaign.campaignData = spreadCampaignData

    updateCampaignsData(spreadCampaign, null, null, setSuccess, message, spreadCampaign._id, callbackDetailCampaign) // disable when testing update component
  }

  const changeCampaignPosition = (idx, type) => {
    const fromIndex = idx
    const message = 'Change Position'
    let toIndex = idx - 1
    if (type === 'down') {
      toIndex = idx + 1
    }
    const spreadCampaign = { ...detailCampaign }
    const spreadCampaignData = [...spreadCampaign.campaignData]
    const element = spreadCampaignData.splice(fromIndex, 1)[0]
    spreadCampaignData.splice(toIndex, 0, element)
    spreadCampaign.campaignData = spreadCampaignData

    updateCampaignsData(spreadCampaign, null, null, setSuccess, message, spreadCampaign._id, callbackDetailCampaign)
  }

  const deleteSelectedCampaign = (idx) => {
    const spreadCampaign = { ...detailCampaign }
    const spreadCampaignData = [...spreadCampaign.campaignData]
    spreadCampaignData.splice(idx, 1)
    spreadCampaign.campaignData = spreadCampaignData

    updateCampaignsData(spreadCampaign, null, null, setSuccess, 'Delete Campaign', spreadCampaign._id, callbackDetailCampaign)
  }

  if (!_.isEmpty(detailCampaign)) {
    return (

      <StateProvider
        deleteSelectedCampaign={deleteSelectedCampaign}
        changeCampaignPosition={changeCampaignPosition}
        expandEditor={expandEditor}
        triggerExpandEditor={triggerExpandEditor}
      // editor
        detailEditorTools={detailEditorTools}
        setEditorType={setEditorType}
        editorType={editorType}
        setDetailEditorToolByType={setDetailEditorToolByType}
        detailEditorToolByType={detailEditorToolByType}
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        // detailCampaign
        detailCampaign={detailCampaign}
        setSuccess={setSuccess}
        // callback
        setDetailCampaign={setDetailCampaign}
        callbackDetailCampaign={callbackDetailCampaign}
        // save
        handleUpdateCampaign={handleUpdateCampaign}
        // navigation
        setActivePreviewIndex={setActivePreviewIndex}
        activePreviewIndex={activePreviewIndex}
      >

        <div className={`detail-item ${expandEditor ? 'expand' : ''}`}>
          <div className='detail-item-header'>
            <CampaignHeader userData={userData} navigateTo={navigateTo} />
          </div>

          <div className={`detail-item-body ${expandEditor ? 'expand' : ''}`}>
            <div className='detail-item-body-left'>
              <CampaignEditor />
            </div>

            {
           expandEditor &&
             <div className='detail-item-body-expand padding-left-l padding-top-m padding-ritgh-m'>
               <CampaignEditorExpand />
             </div>
          }

            <div className='detail-item-body-right'>
              <CampaignDisplay />
            </div>
          </div>
        </div>

      </StateProvider>
    )
  } else {
    return null
  }
}
