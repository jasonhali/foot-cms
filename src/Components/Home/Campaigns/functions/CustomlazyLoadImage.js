import React, { useState } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import 'react-lazy-load-image-component/src/effects/blur.css'
import 'react-loading-skeleton/dist/skeleton.css'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import { toast } from 'react-toastify'

export default function CustomLazyLoadImage ({ image, className, height, width, id, isLoadingImage, prevIsLoading }) {
  // <img src={require('../../Asset/dog.jpg')}

  const [loading, isLoading] = useState(false)

  // const renderSkeleton = () => {
  //   return (
  //     <div>asdsas</div>
  //     // <SkeletonTheme baseColor='#202020' highlightColor='#444'>
  //     //   <p>
  //     //     <Skeleton height={30} width={72} />
  //     //   </p>
  //     // </SkletonTheme>
  //   )
  // }
  const loadFunction = (payload) => {
    isLoading(() => payload,
      prevIsLoading && isLoadingImage(payload)
    )
  }

  return (
    <>
      <LazyLoadImage
        beforeLoad={() => loadFunction(true)}
        afterLoad={() => loadFunction(false)}
        src={loading ? require('../../../../Asset/loading.gif') : image}
        // height={loading ? 10 : 30}
        className={`${loading ? 'isLoading' : className}`}
        effect='blur'
        onError={(e) =>
          toast.error('Image URL tidak Valid')}
        // height={height}
        // width={width}
      // placeholder={<span className='dummy' style={{ color: 'green', height: 400, width: 400 }}>asdsa</span>}
        // placeholderSrc={require('../../../../Asset/dog.jpg')}
        // placeholder={renderSkeleton()}
      />

      {/* {loading && renderSkeleton()} */}
    </>
  )
}
