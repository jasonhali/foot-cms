import React from 'react'

export default function DummyDiv ({ height, width, additionalClass }) {
  return (
    <div
      className={`dummy ${additionalClass || ''}`}
      style={{
        width: width || 100,
        height: height || 20
      }}
    />
  )
}
