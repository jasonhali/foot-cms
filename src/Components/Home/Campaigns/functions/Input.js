
export default function Input ({ value, name, handleOnChange, additionalClass, additionalParams, width, height, type, placeholder, handleOnKeyPress, additionalParamsChange, additionalSecondParams, isReadOnly }) {
  if (handleOnChange) handleOnKeyPress = handleOnChange // parent do not have to send a param again for onKeyPress
  return (
    <input
      id='inputTemplate'
      className={`input ${additionalClass || ''}`}
      style={{
        width: width || '100%',
        height: height || '100%'
      }}
      type={type}
      onChange={(e) => { if (handleOnChange) handleOnChange(e, additionalParams || null, additionalSecondParams || null) }}
      readOnly={isReadOnly && true}
      onKeyUp={(e) => {
        if (handleOnKeyPress) {
          handleOnKeyPress(e, additionalParams, additionalSecondParams || null)
        }
      }}
      name={name}
      value={value}
      placeholder={placeholder}
    />
  )
}
