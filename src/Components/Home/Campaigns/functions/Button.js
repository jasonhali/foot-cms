
export default function Button ({ value, name, handleOnClick, additionalClass, width, height, type, placeholder, icon, iconPosition, size, fontSize, fontWeight }) {
  return (
    <button
      className={`button ${additionalClass !== undefined ? additionalClass : ''} button-size-${size} font-${fontSize} ${fontWeight !== undefined ? `font-weight-${fontWeight}` : ''}`}
      style={{
        width: width || '100%',
        height: height || '100%'
      }}
      type={type}
      onClick={(e) => { if (handleOnClick) handleOnClick(e) }}
      name={name}
      id={name}
      placeholder={placeholder}
    >
      {icon !== undefined && icon}
      {value}
    </button>
  )
}
