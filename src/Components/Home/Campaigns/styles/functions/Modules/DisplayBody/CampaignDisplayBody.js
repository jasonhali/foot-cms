import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'
import ProductComponent from '../Components/ProductComponent'
import _ from 'lodash'
import ImageComponent from '../Components/ImageComponent'
import DividerComponent from '../Components/DividerComponent'

import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import WysiwygIcon from '@mui/icons-material/Wysiwyg'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import Button from '../../../../functions/Button'
import VideoComponent from '../Components/VideoComponent'
import TextComponent from '../Components/TextComponent'
import config from '../../../../../../../config'

export default function CampaignDisplayBody ({ detailCampaign, isLoading }) {
  const [context] = useContext(StateContext)
  const { setActivePreviewIndex, activePreviewIndex, changeCampaignPosition, deleteSelectedCampaign } = context
  const [editorToggle, setEditorToggle] = useState(false)

  // const [isLoading] = context
  // const [isLoading, setIsLoading] = useState([])

  const handleToogleEditorBar = () => {
    setEditorToggle(!editorToggle)
  }

  const components = {
    Product: ProductComponent,
    Image: ImageComponent,
    Divider: DividerComponent,
    Video: VideoComponent,
    Text: TextComponent
  }

  const renderExpandTooltip = (idx, dataArray, dataType) => {
    const isDisableUp = idx === 0 ? '#777779' : '#FEFEFC'
    const isDisableDown = ((idx + 1) === dataArray.length) ? '#777779' : '#FEFEFC'

    return (
      <>
        <Button
          value=''
          type='editor-tooltip'
          additionalClass={`padding-unset ${idx === 0 ? 'isDisable' : ''}`}
          fontSize='s'
          handleOnClick={(e) => idx !== 0 && changeCampaignPosition(idx, 'up')}
          width={40}
          icon={<KeyboardArrowUpIcon sx={{ color: isDisableUp, fontSize: '1.5em' }} />}
        />
        <Button
          value=''
          type='editor-tooltip'
          additionalClass={`padding-unset ${(idx + 1) === dataArray.length ? 'isDisable' : ''}`}
          fontSize='s'
          handleOnClick={(e) => (idx + 1) !== dataArray.length && changeCampaignPosition(idx, 'down')}
          width={40}
          icon={<KeyboardArrowDownIcon sx={{ color: isDisableDown, fontSize: '1.5em' }} />}
        />
        <Button
          value=''
          type='editor-tooltip'
          additionalClass='padding-unset'
          fontSize='s'
          handleOnClick={(e) => deleteSelectedCampaign(idx)}
          width={40}
          icon={<DeleteOutlineIcon sx={{ color: '#FEFEFC', fontSize: '1.5em' }} />}
        />
        {
          dataType === 'Text' &&
            <Button
              value=''
              type='editor-tooltip'
              additionalClass='padding-unset'
              fontSize='s'
              handleOnClick={(e) => handleToogleEditorBar()}
              width={40}
              icon={<WysiwygIcon sx={{ color: '#FEFEFC', fontSize: '1.5em' }} />}
            />
        }
      </>
    )
  }

  const renderBody = () => {
    if (!_.isEmpty(detailCampaign)) {
      return (
        detailCampaign.campaignData.map((data, idx, dataArray) => {
          const RenderComponent = components[data.type]
          return (
            <Fragment key={idx}>

              {RenderComponent &&
                <div key={idx} className={`display-container tooltip  displayIs-${idx === activePreviewIndex ? 'active' : 'unActive'} editor-${data.type === 'Text' ? editorToggle : ''}`} id={data.type.toLowerCase()} onClick={() => setActivePreviewIndex(idx)}>
                  <RenderComponent componentData={data} previewIndex={idx} editorToggle={editorToggle} />
                  <span className={`tooltiptext tooltiptext-${data.type.toLowerCase()}`}>
                    <div className='tooltiptext-expand '>
                      {renderExpandTooltip(idx, dataArray, data.type)}
                    </div>
                  </span>
                </div>}

            </Fragment>
          )
        })
      )
    }
  }

  return (
    <>
      <div className='campaign-display-body'>
        <section className='campaign-display-body-preview'>
          <div id='title' />
          {!isLoading
            ? renderBody()
            : <p>loading</p>}
        </section>
      </div>
    </>

  )
}
