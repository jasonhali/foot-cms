import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'
import OpacityIcon from '@mui/icons-material/Opacity'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import LockIcon from '@mui/icons-material/Lock'
import LockOpenIcon from '@mui/icons-material/LockOpen'
import _ from 'lodash'
import VisibilityIcon from '@mui/icons-material/Visibility'
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff'

import HelpOutlineIcon from '@mui/icons-material/HelpOutline'
import Button from '../../../../functions/Button'

export default function CampaignDisplayHeader (props) {
  const [context] = useContext(StateContext)

  const { activePreviewIndex, detailCampaign } = context

  const [isUnlock, setIsUnlock] = useState(false)

  const renderLeftHeader = () => {
    return (
      <div className='display-header-left'>
        <div className='tooltip tooltip-icon'>
          <HelpOutlineIcon sx={{ color: '#0d1216', fontSize: '1.4em' }} />
          <span className='tooltiptext tooltiptext-help'>Editor Tools</span>
        </div>

        <div className='tooltip-hr'>
          <div className='hr' />
        </div>

        <div className='tooltip-duplicate'>
          <Button
            value='Duplicate'
            type='header-body'
            size='s'
            fontSize='s'
          />
        </div>

      </div>
    )
  }

  const renderMiddleHeader = () => {
    return (
      <div className='display-header-middle'>
        <HelpOutlineIcon sx={{ color: '#FEFEFC', fontSize: 40 }} />
      </div>
    )
  }

  const renderRightHeader = () => {
    return (
      <div className='display-header-right'>
        <div className='tooltip tooltip-unlock'>
          <Button
            value=''
            type='header-body'
            // size='s'
            handleOnClick={() => setIsUnlock(!isUnlock)}
            fontSize='s'
            width={40}
            icon={isUnlock ? <LockOpenIcon sx={{ color: '#0d1216', fontSize: '1.5em' }} /> : <LockIcon sx={{ color: '#0d1216', fontSize: '1.5em' }} />}
          />
          <span className='tooltiptext tooltiptext-delete'>{isUnlock ? 'unlock' : 'lock'}</span>
        </div>

        <div className='tooltip tooltip-delete'>
          <Button
            value=''
            type='header-body'
            // size='s'
            fontSize='s'
            width={40}
            icon={<DeleteOutlineIcon sx={{ color: '#0d1216', fontSize: '1.5em' }} />}
          />
          <span className='tooltiptext tooltiptext-delete'>Delete</span>
        </div>
      </div>
    )
  }

  const renderHeader = () => {
    return (
      <>
        {renderLeftHeader()}
        {renderMiddleHeader()}
        {renderRightHeader()}
      </>
    )
  }

  return (
    <>
      <div className='campaign-display-header'>
        {
          !_.isEmpty(detailCampaign) && renderHeader()
        }
      </div>
    </>

  )
}
