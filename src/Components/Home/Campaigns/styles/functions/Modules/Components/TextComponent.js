import React, { useContext, useEffect, useMemo, useRef, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

import 'swiper/css'

import _ from 'lodash'
import Input from '../../../../functions/Input'
import Skeleton from 'react-loading-skeleton'
import JoditEditor from 'jodit-react'
import config from '../../../../../../../config'

export default function TextComponent ({ editorToggle, componentData, previewIndex, editorBar }) {
  const [context] = useContext(StateContext)
  const [content, setContent] = useState()
  // const [editorConfigManipulate, setEditorConfigManipulate] = useState()
  config.editorConfig.toolbar = true // disable toolbar
  const editorConfig = {
    // buttons: ['bold', 'strikethrough', 'underline', 'italic', 'eraser', '|', 'fontsize', 'align', 'ol', 'brush', '|', 'image', 'video', '|', '|', 'hr', 'symbol']
    // showCharsCounter: false,
    // showWordsCounter: false,
    // showXPathInStatusbar: false
  }
  useEffect(() => {
    if (!_.isEmpty(componentData?.schedule_data?.text)) {
      setContent(componentData.schedule_data.text)
    }
  }, [])

  useEffect(() => {
    if (!_.isEmpty(content)) {
      const spreadComponentdata = { ...componentData }
      const spreadComponentShedule = { ...spreadComponentdata.schedule_data }
      if (spreadComponentShedule.text !== content) {
        spreadComponentShedule.text = content
        spreadComponentdata.schedule_data = spreadComponentShedule
        context.handleUpdateCampaign(spreadComponentdata, 'Update Text', 'text')
      }
    }
  }, [content])

  function handleSetOnBlur (newContent) {
    setContent(newContent)
  }

  return (
    <section className='text' id={componentData.type}>
      <div>
        <JoditEditor
          id='asdsa'
          value={content}
          config={config.editorConfig}
          onBlur={newContent => content !== '' && handleSetOnBlur(newContent)}
        />
      </div>
    </section>

  )
}
