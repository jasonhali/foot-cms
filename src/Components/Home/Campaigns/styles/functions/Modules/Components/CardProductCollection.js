import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

// import CustomLazyLoadImage from '../../../functions/CustomlazyLoadImage'

import { Swiper, SwiperSlide } from 'swiper/react'
import DeleteIcon from '@mui/icons-material/Delete'

import 'swiper/css'
import { Navigation } from 'swiper'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import CustomLazyLoadImage from '../../../../functions/CustomlazyLoadImage'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import Button from '../../../../functions/Button'
import _ from 'lodash'

import SwiperCore, { Autoplay } from 'swiper/core'
import Skeleton from 'react-loading-skeleton'
import DummyDiv from '../../../../functions/DummyDiv'
import Input from '../../../../functions/Input'
import { fetchItemsByValue } from '../../../../../../Services/FetchRequestGroup'
import { LazyLoadImage } from 'react-lazy-load-image-component'

SwiperCore.use([Autoplay])

export default function CardProductCollection ({ cardData, setProduct, idx, setSelectedIndex, idxSKU, idxProduct, setSelectedIndexProduct, clearSKUSection, setClearSKU, setActivePreviewIndex, previewIndex }) {
  const [brandImage, setBrandImage] = useState(null)

  useEffect(() => {
    if (!_.isEmpty(cardData)) {
      if (cardData.brand === 'Nike') {
        setBrandImage('https://res.cloudinary.com/donu8hbzs/image/upload/v1654781564/Content%20Management%20System/Brand%20Logo/Air%20Jordan/AirJordan.png')
      }
    }
  }, [cardData])

  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingItem, setIsLoadingItem] = useState(false)
  const currency = '5300000'

  const handleChangeInput = (e, idx) => {
    setActivePreviewIndex(previewIndex)

    if (e.keyCode === 13 && !_.isEmpty(e.target.value)) {
      // DD1391100
      fetchItemsByValue(e.target.value, setProduct, setIsLoadingItem, null)
    }
  }

  const handleClickClearSKU = (idx) => {
    if (!_.isEmpty(cardData)) {
      setClearSKU(true)
    }
  }

  // const rupiah = () => {
  //   return new Intl.NumberFormat('id-ID', {
  //     style: 'currency',
  //     currency: 'IDR'
  //   }).format(currency)
  // }

  function renderBody () {
    if (_.isEmpty(cardData)) {
      return (
        <div className='card-collection-body' style={{ paddingTop: 'unset', marginBottom: 5 }}>
          <Skeleton height='100%' width='90%' />
        </div>
      )
    } else {
      return (
        <div className='card-collection-body img-collection'>
          <CustomLazyLoadImage
            image={cardData.itemImage[0].imageUrl}
            className='bodyPdp'
          />
        </div>
      )
    }
  }

  function renderFooter (idxSKU, idxProduct) {
    if (_.isEmpty(cardData)) {
      const params = { idxSKU, idxProduct }
      return (
        <>
          <p id='cardTitle' className='margin-top-xs'>
            <Input width='85%' additionalParams={params} height={25} type='actionType' placeholder='input SKU' handleOnKeyPress={handleChangeInput} handleOnChange={handleChangeInput} />
          </p>
          <p id='cardPrice' className='margin-top-s'>
            <Skeleton height={30} width='90%' />
          </p>
        </>
      )
    } else {
      return (
        <>
          <p id='cardTitle'>
            {cardData.itemName}
          </p>
          <p id='cardPrice'>
            {cardData.retailPrice}
            {/* {rupiah().replace(/Rp/i, 'IDR').substring(0, rupiah().length - 2)} */}
          </p>
        </>
      )
    }
  }

  return (
    <>
      <div className={`cardProductCollection ${(idxSKU + 1) % 2 === 0 ? 'zigzag' : ''} ${!_.isEmpty(cardData) ? 'sku-filled' : ''}`} id='cardProductCollection'>
        {!_.isEmpty(cardData) &&
          <div className='clearSku'>
            <div onClick={() => handleClickClearSKU(idxSKU, idxProduct)}>
              <DeleteIcon sx={{ color: '#FEFEFC', fontSize: 40 }} />
            </div>
          </div>}

        <div className='cardProductCollection-header'>
          <div className='cardProductCollection-header-logo'>
            {brandImage &&
              <CustomLazyLoadImage
                image={brandImage}
                className='collection'
              />}
          </div>
        </div>

        {renderBody()}

        <div className='cardProductCollection-footer'>
          {renderFooter(idxSKU, idxProduct)}
        </div>
      </div>
    </>
  )
}
