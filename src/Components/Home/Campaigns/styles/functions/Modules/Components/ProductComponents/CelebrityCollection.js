import React, { Fragment, useContext, useEffect, useState } from 'react'

import 'swiper/css'
import _ from 'lodash'
import { StateContext } from '../../../../../../../StateContext'
import Skeleton from 'react-loading-skeleton'
import Input from '../../../../../functions/Input'
import CustomLazyLoadImage from '../../../../../functions/CustomlazyLoadImage'
import JoditEditor from 'jodit-react'

import config from '../../../../../../../../config'
import CardProductCollection from '../CardProductCollection'

export default function CelebrityCollection ({ componentData, previewIndex }) {
  config.editorConfig2.toolbar = false // disable toolbar
  const [context] = useContext(StateContext)
  // const [configEditor] = useState(config.editorConfig)
  // configEditor.toolbar = false

  const [collabrorationData, setCollabrorationData] = useState()

  useEffect(() => {
    if (!_.isEmpty(componentData)) {
      setCollabrorationData(componentData)
    }
  }, [])

  const [activeIndex] = useState(0)

  function renderHeaderComponent () {
    return (
      <Input
        width='60%' type='transparent' placeholder='Input Product Title'
        value={collabrorationData?.title || ''}
        handleOnChange={handleChangeData}
        additionalParams='title'
      />
    )
  }

  function handleChangeData (e, type) {
    const { handleUpdateCampaign } = context
    let message

    if (collabrorationData) {
      const spreadComponent = { ...collabrorationData }

      if (type === 'title') {
        spreadComponent.title = e.target.value
      } else if (type === 'name') {
        const spreadComponentImage = { ...spreadComponent.product_data }

        spreadComponentImage.title = e.target.value
        spreadComponent.product_data = spreadComponentImage
      } else {
        spreadComponent.image_url = e.target.value
        message = 'Image'
      }

      setCollabrorationData(spreadComponent)
      if (e.keyCode === 13) {
        handleUpdateCampaign(spreadComponent, `Ubah ${message}`, 'celebrity-collection')
      }
    }
  }

  function renderInputHover (type) {
    //   'https://res.cloudinary.com/donu8hbzs/image/upload/v1653570213/Content%20Management%20System/Artist/Jennifer%20lopez/Frame/jennifer_lopez_p5usiz.png'
    return (
      <div className='celebrity-collection-input'>

        {collabrorationData?.image_url === ''
          ? <Skeleton width='85%' />
          : type === 'image'
            ? <CustomLazyLoadImage
                image={collabrorationData?.image_url}
                className='collection'
              />
            : null}

        <div className='celebrity-collection-input-background'>
          <Input
            width='60%' height={25} type='actionType' placeholder='Input image URL'
            handleOnChange={handleChangeData}
            value={collabrorationData?.image_url || ''}
          />
        </div>

      </div>
    )
  }

  function renderLeftBodyComponent () {
    // 'https://dummyimage.com/600x900/700999/700999'
    return (
      <>
        <div
          className='celebrity-collection-leftBody-collection bottom-image'
          style={{
            zIndex: activeIndex
          }}
        >
          {renderInputHover('image')}
        </div>
      </>
    )
  }

  const [product, setProduct] = useState([])

  const [clearSKU, setClearSKU] = useState(false)

  useEffect(() => {
    if (clearSKU) {
      triggerUpdateWhenAction({}, 'Clear SKU')
    }
  }, [clearSKU])

  useEffect(() => {
    if (!_.isEmpty(product)) {
      triggerUpdateWhenAction(product[0], 'Insert SKU')
    }
  }, [product])

  function triggerUpdateWhenAction (payload, message) {
    const spreadData = { ...collabrorationData }
    const spreadDataCollab = { ...spreadData.product_data }

    spreadDataCollab.product = payload
    spreadData.product_data = spreadDataCollab

    context.handleUpdateCampaign(spreadData, message, 'celebrity-collection')
  }

  const handleChangeEditor = (editor) => {
    const spreadData = { ...componentData }
    const spreadDataCollab = { ...spreadData.product_data }

    spreadDataCollab.description = editor
    spreadData.product_data = spreadDataCollab
    context.handleUpdateCampaign(spreadData, 'Update Description', 'celebrity-collection')
  }

  function renderRightBodyComponent () {
    const { setActivePreviewIndex } = context

    return (
      <>
        <div className='rightBody-top margin-top-s'>
          <Input
            width='60%' type='transparent' placeholder='Input Product Title'
            value={collabrorationData?.product_data?.title || ''}
            handleOnChange={handleChangeData}
            additionalParams='name'
          />
        </div>

        <div className='rightBody-middle margin-top-s'>
          <CardProductCollection
            type='celebrity-collection'
            setClearSKU={setClearSKU}
            cardData={collabrorationData?.product_data?.product}
            setProduct={setProduct}
            setActivePreviewIndex={setActivePreviewIndex}
            previewIndex={previewIndex}
          />
          {/* ))} */}
        </div>
        <div className='rightBody-bottom margin-top-s'>
          <JoditEditor
            value={collabrorationData?.product_data?.description || ''}
            config={config.editorConfig2}
            onBlur={(newContent) => handleChangeEditor(newContent)}
          />
        </div>
      </>
    )
  }

  return (
    <div className='product celebrity-collection' id='celebrity-collection'>
      <div className='celebrity-collection-header margin-top-xs'>
        {renderHeaderComponent()}
      </div>

      <div className='celebrity-collection-container margin-top-s'>

        <div className='celebrity-collection-leftBody '>
          {renderLeftBodyComponent()}
        </div>

        <div className='celebrity-collection-rightBody'>
          {renderRightBodyComponent()}
        </div>

      </div>
    </div>

  )
}
