import React, { Fragment, useContext, useEffect, useState } from 'react'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { Navigation } from 'swiper'

// import CardProduct from './CardProduct'
import _ from 'lodash'
import { StateContext } from '../../../../../../../StateContext'
import Skeleton from 'react-loading-skeleton'
import Input from '../../../../../functions/Input'
import CustomLazyLoadImage from '../../../../../functions/CustomlazyLoadImage'
import CardProductCollaboration from '../CardProductCollaboration'
import JoditEditor from 'jodit-react'

import config from '../../../../../../../../config'

export default function ProductCollaboration ({ componentData, previewIndex }) {
  config.editorConfig.toolbar = false // disable toolbar
  const [context] = useContext(StateContext)

  const [activeIndex, setActiveIndex] = useState(0)
  const [imageUrl, setImageUrl] = useState('')
  const [signatureUrl, setSignatureUrl] = useState('')
  const [artistName, setArtistName] = useState('')
  const [productTitle, setProductTitle] = useState('')
  const [description, setDescription] = useState('')

  const [collaborationProduct, setCollaborationProduct] = useState([])

  useEffect(() => {
    if (componentData) {
      if (componentData?.collaboration?.[activeIndex]?.image_url) {
        setImageUrl(componentData.collaboration[activeIndex].image_url)
      }
      // title
      if (componentData.title) setProductTitle(componentData.title)
      else setProductTitle('The Biggest and Best Collaborations')
      // name
      if (componentData?.collaboration?.[activeIndex]?.name) {
        setArtistName(componentData.collaboration[activeIndex].name)
      } else {
        setArtistName('')
      }
      // sigature
      if (componentData?.collaboration?.[activeIndex]?.signature_image_url) {
        setSignatureUrl(componentData.collaboration[activeIndex].signature_image_url)
      } else {
        setSignatureUrl('')
      }

      // text
      if (componentData?.collaboration?.[activeIndex]?.description) {
        setDescription(componentData.collaboration[activeIndex].description)
      } else {
        setDescription('')
      }

      if (componentData?.collaboration?.[activeIndex].product) {
        setCollaborationProduct(componentData.collaboration[activeIndex].product)
      }
    }
  }, [activeIndex])

  useEffect(() => {
    if (!_.isEmpty(description)) {
      const spreadData = { ...componentData }
      const spreadDataCollab = [...spreadData.collaboration]
      const spreadCollabDetail = { ...spreadDataCollab[activeIndex] }

      if (spreadCollabDetail.description !== description) {
        spreadCollabDetail.description = description
        spreadDataCollab[activeIndex] = spreadCollabDetail
        spreadData.collaboration = spreadDataCollab
        context.handleUpdateCampaign(spreadData, 'Update Description', 'product-collaboration')
      }
    }
  }, [description])

  function renderHeaderComponent () {
    return (
      // <input value={productTitle} onChange={(e) => handleChangeInput(e, 'title')} />
      <Input
        width='60%' type='transparent' placeholder='Input youtube URL'
        value={productTitle}
        handleOnChange={handleChangeInput}
        additionalParams=''
        additionalSecondParams='title'
      />
    )
  }

  const handleChangeInput = (e, index, target) => {
    if (target === 'title') {
      setProductTitle(e.target.value)
    } else if (target === 'signature') {
      setSignatureUrl(e.target.value)
    } else if (target === 'name') {
      setArtistName(e.target.value)
    } else {
      setImageUrl(e.target.value)
    }

    const { handleUpdateCampaign } = context
    if (e.keyCode === 13) {
      let message
      if (index === null) {
        index = 0
      }
      const spreadData = { ...componentData }

      if (target === 'title') {
        spreadData.title = e.target.value
        message = 'Product title'
      } else {
        const spreadDataCollab = [...spreadData.collaboration]
        const spreadCollabDetail = { ...spreadDataCollab[index] }
        if (target === 'name') {
          spreadCollabDetail.name = e.target.value
          message = 'Nama'
        } else if (target === 'signature') {
          spreadCollabDetail.signature_image_url = e.target.value
          message = 'Signature'
        } else {
          spreadCollabDetail.image_url = e.target.value
          message = 'Image Url'
        }
        spreadDataCollab[index] = spreadCollabDetail
        spreadData.collaboration = spreadDataCollab
      }

      handleUpdateCampaign(spreadData, `Ubah ${message}`, 'product-collaboration')
    }
  }

  function renderLeftBodyComponent () {
    // componentData.collaboration
    // const image1 = 'https://res.cloudinary.com/donu8hbzs/image/upload/v1652617881/Content%20Management%20System/Artist/Travis/Images/travis.png'
    // https://res.cloudinary.com/donu8hbzs/image/upload/v1653106627/Content%20Management%20System/Artist/Travis/Signature/signature_ysosrh.png
    // const image2 = 'https://res.cloudinary.com/donu8hbzs/image/upload/v1652618136/Content%20Management%20System/Artist/Billie/Images/billie.png'
    // https://res.cloudinary.com/donu8hbzs/image/upload/v1652617038/Content%20Management%20System/Artist/Billie/Signature/signature_m2al4v.png

    return (
      componentData.collaboration.map((data, idx) => (
        <Fragment key={idx}>
          <div
            className={`product-collaboration-leftBody-collaboration ${idx === 0 ? 'top' : 'bottom'}-image  ${idx === activeIndex ? 'active' : ''}`}
            style={{
              opacity: idx === activeIndex ? 1 : 0.5,
              zIndex: activeIndex,
              filter: `grayscale(${idx === activeIndex ? '0%' : '100%'})`
            }}
            onClick={() => setActiveIndex(idx)}
          >
            <CustomLazyLoadImage
              image={data.image_url || 'https://dummyimage.com/600x900/700999/700999'}
              className='collaboration'
            />
          </div>
          <Input
            value={imageUrl}
            additionalClass={`collabroration-text text-${idx === 0 ? 'top' : 'bottom'}`} type='actionType' placeholder='Input image url' handleOnChange={handleChangeInput} additionalParams={idx} handleOnKeyPress={handleChangeInput}
          />
        </Fragment>
      ))
    )
  }

  const [product, setProduct] = useState([])

  const [clearSKU, setClearSKU] = useState(false)
  const [selectedIndex, setSelectedIndex] = useState(0)

  const [selectedIndexProduct, setSelectedIndexProduct] = useState(0)
  useEffect(() => {
    if (clearSKU) {
      triggerUpdateWhenAction({}, 'Clear SKU')
    }
  }, [clearSKU])

  useEffect(() => {
    if (!_.isEmpty(product)) {
      triggerUpdateWhenAction(product[0], 'Insert SKU')
    }
  }, [product])

  function triggerUpdateWhenAction (payload, message) {
    const spreadData = { ...componentData }
    const spreadDataCollab = [...spreadData.collaboration]
    const spreadCollabDetail = { ...spreadDataCollab[activeIndex] }
    const spreadCollabProduct = [...spreadCollabDetail.product]
    let spreadProductOverride = { ...spreadCollabProduct[selectedIndexProduct] }

    spreadProductOverride = payload
    spreadCollabProduct[selectedIndexProduct] = spreadProductOverride
    spreadCollabDetail.product = spreadCollabProduct

    spreadDataCollab[activeIndex] = spreadCollabDetail
    spreadData.collaboration = spreadDataCollab

    context.handleUpdateCampaign(spreadData, message, 'product-collaboration')
  }

  function renderRightBodyComponent () {
    const { setActivePreviewIndex } = context
    return (
      <>
        <div className='rightBody-top margin-top-l'>
          {collaborationProduct.map((dataProduct, idxSKU) => (
            <CardProductCollaboration
              key={idxSKU}
              type='product-collaboration'
              setClearSKU={setClearSKU}
              setSelectedIndexProduct={setSelectedIndexProduct}
              cardData={dataProduct}
              idxSKU={idxSKU}
              setProduct={setProduct}
              setSelectedIndex={setSelectedIndex}
              setActivePreviewIndex={setActivePreviewIndex}
              previewIndex={previewIndex}
            />
          ))}
        </div>
        <div className='rightBody-bottom margin-top-s'>
          <JoditEditor
            value={description}
            config={config.editorConfig}
            onBlur={newContent => setDescription(newContent)}
          />
        </div>
      </>
    )
  }

  function renderfooterComponent () {
    return (
      <>
        <Input
          width='100%' type='transparent' placeholder='Input Artist Name'
          value={artistName}
          handleOnChange={handleChangeInput}
          additionalParams={activeIndex}
          additionalSecondParams='name'
        />
        <div className='signature'>
          <Input
            type='actionType' placeholder='Input Signature'
            value={signatureUrl}
            handleOnChange={handleChangeInput}
            additionalParams={activeIndex}
            additionalSecondParams='signature'
          />
          {
            signatureUrl !== ''
              ? <CustomLazyLoadImage
                  image={signatureUrl || 'https://dummyimage.com/600x900/700999/700999'}
                  className='collaboration'
                />
              : <Skeleton height={80} width={80} />
          }
        </div>
      </>
    )
  }

  return (
    <div className='product product-collaboration' id='product-collaboration' style={{ background: 'linear-gradient(180deg, #18191B 0%, #292254 51.56%, #18191B 100%)' }}>

      <div className='product-collaboration-header margin-top-xs'>
        {renderHeaderComponent()}
      </div>

      <div className='product-collaboration-container margin-top-xl'>

        <div className='product-collaboration-leftBody '>
          {renderLeftBodyComponent()}
        </div>

        <div className='product-collaboration-rightBody'>
          {renderRightBodyComponent()}
        </div>

      </div>

      <div className='product-collaboration-bottom margin-top-m'>
        {renderfooterComponent()}
      </div>
    </div>

  )
}
