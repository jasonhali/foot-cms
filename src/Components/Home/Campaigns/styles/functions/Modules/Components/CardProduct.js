import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

// import CustomLazyLoadImage from '../../../functions/CustomlazyLoadImage'

import { Swiper, SwiperSlide } from 'swiper/react'
import DeleteIcon from '@mui/icons-material/Delete'

import 'swiper/css'
import { Navigation } from 'swiper'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import CustomLazyLoadImage from '../../../../functions/CustomlazyLoadImage'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import Button from '../../../../functions/Button'
import _ from 'lodash'

import SwiperCore, { Autoplay } from 'swiper/core'
import Skeleton from 'react-loading-skeleton'
import DummyDiv from '../../../../functions/DummyDiv'
import Input from '../../../../functions/Input'
import { fetchItemsByValue } from '../../../../../../Services/FetchRequestGroup'

SwiperCore.use([Autoplay])

export default function CardProduct ({ cardData, setProduct, idx, setSelectedIndex, idxSKU, idxProduct, setSelectedIndexProduct, clearSKUSection, setClearSKU, setActivePreviewIndex, previewIndex }) {
  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingItem, setIsLoadingItem] = useState(false)
  const currency = '5300000'

  const handleChangeInput = (e, idx) => {
    setActivePreviewIndex(previewIndex)

    if (e.keyCode === 13 && !_.isEmpty(e.target.value)) {
      // DD1391100
      setSelectedIndex(() => idx.idxSKU,
        setSelectedIndexProduct(idx.idxProduct),
        fetchItemsByValue(e.target.value, setProduct, setIsLoadingItem, null)
      )
    }
  }

  const handleClickClearSKU = (idx) => {
    if (!_.isEmpty(cardData)) {
      setSelectedIndex(() => idxSKU,
        setSelectedIndexProduct(() => idxProduct,
          setClearSKU(true)
        // clearSKUSection(idxSKU,idxProduct)
        )
      )
    }
  }

  // const rupiah = () => {
  //   return new Intl.NumberFormat('id-ID', {
  //     style: 'currency',
  //     currency: 'IDR'
  //   }).format(currency)
  // }

  function renderHeader (idx) {
    if (_.isEmpty(cardData)) {
      return (
        <>
          <Skeleton height={25} width={25} />
          <Skeleton height={25} width={25} />
        </>
      )
    } else {
      return (
        <>
          <CustomLazyLoadImage
            image={require('../../../../../../../Asset/pdp/airJordan.png')}
            className='logoPdp'
          />

          <Button
            value=''
            type='drop-shadow'
            additionalClass='margin-top-xs wishlist'
            iconAdditionalClass='margin-top-xs'
            size='s'
            fontSize='s'
            name='campaign-header'
            fontWeight='bold'
            height='unset'
            width=''
            // handleOnClick={() => handleOnClickButton(dataType)}
            icon={<FavoriteBorderIcon className='wishlist' sx={{ fontSize: 20 }} />}
          />
        </>
      )
    }
  }

  function renderBody () {
    if (_.isEmpty(cardData)) {
      return (
        <div className='cardProduct-body' style={{ paddingTop: 'unset', marginBottom: 7 }}>
          <Skeleton height='100%' width='90%' />
        </div>
      )
    } else {
      return (
        <div className='cardProduct-body'>
          <CustomLazyLoadImage
            image={cardData.itemImage[0].imageUrl}
            // image={require('../../../../../../../Asset/pdp/dummySneakers.png')}
            className='bodyPdp'
          />
        </div>
      )
    }
  }

  function renderFooter (idxSKU, idxProduct) {
    if (_.isEmpty(cardData)) {
      const params = { idxSKU, idxProduct }
      return (
        <>
          <p id='cardTitle'>
            <Input width='85%' additionalParams={params} height={25} type='actionType' placeholder='input SKU' handleOnKeyPress={handleChangeInput} handleOnChange={handleChangeInput} />
          </p>
          <p id='cardPrice'>
            <Skeleton height={30} width='90%' />
          </p>
        </>
      )
    } else {
      return (
        <>
          <p id='cardTitle'>
            {cardData.itemName}
          </p>
          <p id='cardPrice'>
            {cardData.retailPrice}
          </p>
        </>
      )
    }
  }

  return (
    <>
      <div className={`cardProduct ${(idxSKU + 1) % 2 === 0 ? 'zigzag' : ''} ${!_.isEmpty(cardData) ? 'sku-filled' : ''}`} id='cardProduct'>
        {!_.isEmpty(cardData) &&
          <div className='clearSku'>
            <div onClick={() => handleClickClearSKU(idxSKU, idxProduct)}>
              <DeleteIcon sx={{ color: '#FEFEFC', fontSize: 40 }} />
            </div>
          </div>}

        <div className='cardProduct-header'>
          {renderHeader()}
        </div>

        {renderBody()}

        <div className='cardProduct-footer'>
          {renderFooter(idxSKU, idxProduct)}
        </div>
      </div>
    </>
  )
}
