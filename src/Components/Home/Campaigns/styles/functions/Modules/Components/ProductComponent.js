import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { Navigation } from 'swiper'

import CardProduct from './CardProduct'
import _ from 'lodash'
import ProductCollaboration from './ProductComponents/ProductCollaboration'
import CelebrityCollection from './ProductComponents/CelebrityCollection'

export default function ProductComponent ({ componentData, previewIndex }) {
  const [context] = useContext(StateContext)
  // const [dummy, setDummy] = useState([{}, {}, {}, {}, {}, {}])
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [selectedIndexProduct, setSelectedIndexProduct] = useState(0)

  const { handleUpdateCampaign, setActivePreviewIndex } = context
  const [product, setProduct] = useState([])

  const [clearSKU, setClearSKU] = useState(false)

  useEffect(() => {
    if (clearSKU) {
      triggerUpdateWhenAction({}, 'Clear SKU')
    }
  }, [clearSKU])

  useEffect(() => {
    if (!_.isEmpty(product)) {
      triggerUpdateWhenAction(product[0], 'Insert SKU')
    }
  }, [product])

  function triggerUpdateWhenAction (payload, message) {
    const manipulateProduct = { ...componentData }
    const manipulateProductData = [...manipulateProduct.product]
    const manipulateProductDataindex = [...manipulateProductData[selectedIndexProduct]]

    manipulateProductDataindex[selectedIndex] = payload
    manipulateProductData[selectedIndexProduct] = manipulateProductDataindex

    manipulateProduct.product = manipulateProductData
    handleUpdateCampaign(manipulateProduct, message, 'product')
  }

  function renderProductComponent (type) {
    type = type.split(' ').join('-') // name from db is Product Collaboration, so the white space will be replace using "-"

    switch (type) {
      case 'product-collaboration' : return <ProductCollaboration componentData={componentData} previewIndex={previewIndex} />
      case 'product-collection' : return <CelebrityCollection componentData={componentData} previewIndex={previewIndex} />
    }
  }

  return (
    <>
      <section className='product campaign-display-body-preview-product' id={componentData.name.split(' ').join('-')}>
        {
        componentData.name === 'product' &&
          <>
            <div id='gradient' />
            <Swiper
              loop
          // modules={[Navigation]}
          // navigation
              resistanceRatio={0}
              slidespercolumn={1}
              slidespercolumnfill='row'
              slidesPerView={6}
              spaceBetween={160}
            >
              {componentData.product.map((data, idxProduct) => (
                <SwiperSlide key={idxProduct}>
                  <div className='product-container'>
                    {data.map((dataProduct, idxSKU) => (
                      <CardProduct
                        setClearSKU={setClearSKU}
                        setSelectedIndexProduct={setSelectedIndexProduct}
                        key={idxSKU}
                        cardData={dataProduct}
                        idxSKU={idxSKU}
                        idxProduct={idxProduct}
                        setProduct={setProduct}
                        setSelectedIndex={setSelectedIndex}
                        setActivePreviewIndex={setActivePreviewIndex}
                        previewIndex={previewIndex}
                      />
                    ))}
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </>
        }

        {
          renderProductComponent(componentData.name)
        }

      </section>

    </>

  )
}
