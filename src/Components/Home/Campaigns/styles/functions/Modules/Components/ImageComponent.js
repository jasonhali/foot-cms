import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { Navigation } from 'swiper'

import _ from 'lodash'
import CustomLazyLoadImage from '../../../../functions/CustomlazyLoadImage'
import Input from '../../../../functions/Input'
import Skeleton from 'react-loading-skeleton'

export default function ImageComponent ({ componentData, previewIndex }) {
  const [context] = useContext(StateContext)

  const [imageUrl, setImageUrl] = useState('')

  useEffect(() => {
    if (componentData?.schedule_data?.[0]?.img_url) {
      setImageUrl(componentData?.schedule_data?.[0]?.img_url)
    }
  }, [])

  const handleChangeImageUrl = (e) => {
    context.setActivePreviewIndex(previewIndex)

    setImageUrl(e.target.value)

    if (e.keyCode === 13 && !_.isEmpty(imageUrl)) {
      const manipulateImage = { ...componentData }
      const manipulateImageData = [...manipulateImage.schedule_data]
      const manipulateImageDataIndex = { ...manipulateImageData[0] }
      manipulateImageDataIndex.img_url = imageUrl
      manipulateImageData[0] = manipulateImageDataIndex
      manipulateImage.schedule_data = manipulateImageData
      context.handleUpdateCampaign(manipulateImage, 'Add Url', 'image')
    }
  }
  // https://res.cloudinary.com/donu8hbzs/image/upload/v1651568425/samples.png

  return (
    componentData?.schedule_data.map((data, idx) => (
      <section className='image' id={componentData.type} key={idx}>

        <div className='image-component'>
          <div className='input-field'>
            <Input width='60%' height={25} type='actionType' placeholder='Input image URL' handleOnKeyPress={handleChangeImageUrl} handleOnChange={handleChangeImageUrl} value={imageUrl} />
          </div>

          {
          !_.isEmpty(data.img_url)
            ? <CustomLazyLoadImage
                image={data.img_url}
                className='logoPdp'
              />
            : <div className='empty'>
              <Skeleton
                height={100} width='80%'
              // baseColor='#202020'
                highlightColor='#FEFEFC'
              />
            </div>
            }
        </div>
      </section>
    ))
  )
}
