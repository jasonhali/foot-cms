import React, { useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

import 'swiper/css'

import _ from 'lodash'
import Input from '../../../../functions/Input'
import Skeleton from 'react-loading-skeleton'

export default function VideoComponent ({ componentData, previewIndex }) {
  const [context] = useContext(StateContext)

  const [videoUrl, setVideoUrl] = useState('')

  useEffect(() => {
    if (componentData?.schedule_data?.[0]?.video_url) {
      setVideoUrl(componentData?.schedule_data?.[0]?.video_url)
    }
  }, [])

  const handleChangeVideoUrl = (e) => {
    context.setActivePreviewIndex(previewIndex)

    setVideoUrl(e.target.value)

    if (e.keyCode === 13 && !_.isEmpty(videoUrl)) {
      const manipulateVideo = { ...componentData }
      const manipulateVideoData = [...manipulateVideo.schedule_data]
      const manipulateVideoDataIndex = { ...manipulateVideoData[0] }

      manipulateVideoDataIndex.video_url = videoUrl.replace('watch?v=', 'embed/')
      manipulateVideoData[0] = manipulateVideoDataIndex
      manipulateVideo.schedule_data = manipulateVideoData
      context.handleUpdateCampaign(manipulateVideo, 'Add Url', 'video')
    }
  }

  return (
    componentData?.schedule_data.map((data, idx) => (
      <section className='video' id={componentData.type} key={idx}>

        <div className='video-component'>
          <div className='input-field'>
            <Input width='60%' height={25} type='actionType' placeholder='Input youtube URL' handleOnKeyPress={handleChangeVideoUrl} handleOnChange={handleChangeVideoUrl} value={videoUrl} />
          </div>

          {
          !_.isEmpty(data.video_url)
            ? <iframe className='logoPdp' src={data.video_url} />

            : <div className='empty'>
              <Skeleton
                width='100%'
              // baseColor='#202020'
                highlightColor='#FEFEFC'
              />
            </div>
            }
        </div>
      </section>
    ))
  )
}
