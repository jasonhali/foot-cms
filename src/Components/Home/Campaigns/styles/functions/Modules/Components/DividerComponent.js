import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../../../../../StateContext'

export default function DividerComponent ({ componentData, previewIndex }) {
  return (
    <section className={`divider divider-${componentData.spacing}`} id={componentData.type} />
  )
}
