import React, { useContext } from 'react'
import LeftComponent from './Home Component/LeftComponent'

import RightComponent from './Home Component/RightComponent'
import { StateContext } from '../StateContext'
import _ from 'lodash'

export default function Body (props) {
  const [context] = useContext(StateContext)
  const { type, navigation } = props

  return (
    <div className='flex center h-ex-full w-full'>
      <div className='Header w-ex-small main-tab-left main-tab-left-radius h-full border-right'>
        <LeftComponent context={context} />
      </div>

      <div className='w-full main-tab-right h-full center'>
        {context.isLoading
          ? <img src={require('../../Asset/loading.gif')} height={90} style={{ paddingLeft: '10%', maxHeight: 40 }} alt='Italian Trulli' />
          : <RightComponent
              campaigns={context.campaigns}
              handleDeleteCampaign={context.onHandleDeleteCampaign}
              type={type}
            />}
      </div>

    </div>
  )
}
