import React, { Fragment } from 'react'

import Header from './Header'
import Body from './Body'

// const $ = window.$

export default function Home (props) {
  const { campaigns, isLoading, type, setKeyword, filterData, keyword, navigation } = props

  return (
    <>
      <Header
        setKeyword={setKeyword}
        filterData={filterData}
        keyword={keyword}
        navigation={navigation}
      />
      <Body
        isLoading={isLoading}
        campaigns={campaigns}
        type={type}
        navigation={navigation}
      />
    </>
  )
}
