import React, { Fragment, useContext } from 'react'
import _ from 'lodash'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'
import { formatDate, parseDate } from 'react-day-picker/moment'
import moment from 'moment'
import { StateContext } from '../../StateContext'
import { fetchGetActiveCampaignUrl } from '../../../Components/Services/FetchRequestGroup'

export default function CampaignModal (props) {
  const [context] = useContext(StateContext)

  const {
    displayModal,
    inputField,
    dateField,
    startDate,
    endDate,
    setStartDate,
    setEndDate,
    setCampaignName,
    setTitle,
    setUrlKey,
    handleSubmitCampaign,
    campaignName,
    urlKey,
    title,
    actionClick,
    isActiveUrl,
    setIsLoading,
    checkIsActiveUrl
  } = context
  // isActiveUrl === true kalau match dgn url yg sdh ada
  function handleChangeUrl (dataOnChanges, e) {
    dataOnChanges(e.target.value)
  }

  function renderInputField (data, idx) {
    const dataOnChanges = data.value === 'title' ? setTitle : data.value === 'urlKey' ? setUrlKey : setCampaignName

    const dataValue = data.value === 'title' ? title : data.value === 'urlKey' ? urlKey : campaignName

    return (
      <div id='m-field' style={{ justifyContent: idx === 0 ? 'end' : idx === 1 ? 'center' : 'start' }}>
        <div>{`Content ${data.name}`}</div>
        {idx === 2
          ? <div className='validate'>
            <input id='m-input' value={dataValue} className={`input-left ${isActiveUrl ? 'err' : ''}`} placeholder={`Input Content ${data.name}`} onChange={e => handleChangeUrl(dataOnChanges, e)} />
            </div>
          : <input id='m-input' value={dataValue} placeholder={`Input Content ${data.name}`} onChange={e => dataOnChanges(e.target.value)} />}
      </div>
    )
  }

  function renderDatePick () {
    const dateState = {
      startDate: startDate,
      endDate: endDate
    }
    return (
      <div className='h-full w-full'>
        {!_.isEmpty(context) &&
        dateField.map((data, index) => {
          const name = data.value === 'startDate' ? setStartDate : setEndDate
          return (
            <div className={`w-half column center ${index !== 0 ? 'end' : ''}`} id='picker' key={index}>
              <div>{data.name}</div>
              <DayPickerInput
                format='MM/DD/YYYY'
                formatDate={formatDate}
                parseDate={parseDate}
                value={dateState[data.value]}
                onDayChange={day => name(moment(day).format('MM/DD/YYYY'))}
                dayPickerProps={{
                  disabledDays: { before: new Date() }
                }}
              />
            </div>
          )
        }
        )}
      </div>
    )
  }

  return (
    <>
      <div id='modal-header' className='column center'>
        <div id='modal-title' className='w-full center flex'>ADD ITEM</div>
        <hr id='m-hr' />
      </div>

      <div id='modal-body' className='w-full flex'>
        <div className='h-full flex center' id='m-content-left'>
          {inputField.map((data, idx) => (
            <Fragment key={idx}>
              {renderInputField(data, idx)}
            </Fragment>
          ))}
        </div>

        <div className='h-full w-half' id='m-content-right'>
          {renderDatePick()}
        </div>
      </div>

      <div id='modal-footer' className='w-full flex a-start center'>
        <button onClick={handleSubmitCampaign}>{actionClick.toUpperCase()}
        </button>
      </div>
    </>
  )
}
