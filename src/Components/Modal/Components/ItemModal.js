import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../../StateContext'

import DayPickerInput from 'react-day-picker/DayPickerInput'
import 'react-day-picker/lib/style.css'

import { formatDate, parseDate } from 'react-day-picker/moment'
import moment from 'moment'
import _ from 'lodash'
import AddIcon from '@mui/icons-material/Add'
import DeleteIcon from '@mui/icons-material/Delete'
import ImageIcon from '@mui/icons-material/Image'
import ImageModal from './Containers/ImageModal'
import { addNewItems, updateItems } from '../../Services/FetchRequestGroup'
import { toast } from 'react-toastify'

export default function ItemModal () {
  const [actionClick, changeActionClick] = useState('Submit')

  const [context] = useContext(StateContext)
  const [itemConditionOption] = useState([{ name: 'BNIB', value: 'BNIB' }, { name: 'VNDS', value: 'VNDS' }])
  const [brandOption] = useState([
    { name: 'Nike', value: 'Nike' },
    { name: 'Air Jordan', value: 'air-jordan' },
    { name: 'Adidas', value: 'adidas' },
    { name: 'New Balance', value: 'new-balance' }])
  const [itemTypeOption] = useState([{ name: 'Sneakers', value: 'sneakers' }, { name: 'Clothes', value: 'clothes' }, { name: 'Other', value: 'other' }])
  const [genderOption] = useState([{ name: 'Male', value: 'male' }, { name: 'Female', value: 'female' }])
  const [ratingOption] = useState([
    { name: '1', value: '1' },
    { name: '2', value: '2' },
    { name: '3', value: '3' },
    { name: '4', value: '4' },
    { name: '5', value: '5' }
  ])
  const [itemSize, handleChangeSize] = useState([])

  // images
  const [itemImages, handleSetImages] = useState([
    { imageUrl: '' }
  ])

  // active Index Size
  const [activeIndexSize, handleChangeActiveIndexSize] = useState(0)
  const [sizeDetailData, handleChangeSizeDetailData] = useState([])
  const [activeIndexSizeDetail, handleChangeActiveIndexSizeDetail] = useState(0)

  // SetState
  // left
  const [name, handleChangeItemName] = useState('')
  const [sku, handleChangeSku] = useState('')
  const [retailPrice, handleChangeRetailPrice] = useState('')
  const [colorway, handleChangeColorway] = useState('')

  // option left
  const [condition, handleChangeCondition] = useState('BNIB')
  const [rating, handleChangeRating] = useState(5)
  const [type, handleChangeType] = useState('Sneakers')
  const [gender, handleChangeGender] = useState('Male')
  const [brand, handleChangeBrand] = useState('Nike')
  const [releaseDate, handleChangeDate] = useState(moment(new Date()).format('MM/DD/YYYY'))

  // middle
  const [itemDescription, handleChangeDescription] = useState('')
  // right
  // const [modalRme, handleChangeModalRme] = useState(0)
  // const [kurs, handleChangeKurs] = useState(0)
  // const [ongkir, handleChangeOngkir] = useState(0)
  // const [modal, handleChangeModal] = useState(0)
  // const [jual, handleChangeJual] = useState(0)
  // const [profit, handleChangeProfit] = useState(0)
  // const [totalModal, handleChangeTotalModal] = useState(0)

  const [showimageModal, handleTriggerImageModal] = useState(false)
  const [imageLink, handleImageLink] = useState('')

  // const
  // post modal item
  const [success, setSuccess] = useState({ success: false, data: {} })

  const {
    handleSubmitCampaign,
    getAllItems,
    triggerModal,
    itemId,
    itemData,
    displayModal,
    fromBody
  } = context

  useEffect(() => {
    if (!_.isEmpty(itemData) && fromBody) {
      handleChangeItemName(itemData.itemName)
      handleChangeSku(itemData.sku)
      handleChangeRetailPrice(itemData.retailPrice)

      handleChangeColorway(itemData.colorway)

      handleChangeCondition(itemData.itemCondition)
      handleChangeRating(itemData.itemRating)
      handleChangeGender(itemData.gender)
      handleChangeSize(itemData.itemSize)
      handleChangeType(itemData.itemType)
      handleChangeBrand(itemData.brand)
      handleChangeDate(itemData.releaseDate)
      handleSetImages(itemData.itemImage)
      handleChangeDescription(itemData.itemDescription)
      changeActionClick('Update')
    }
  }, [])

  function getCheckedCheckboxesFor (data, e) {
    if (e.target.checked) {
      handleChangeSize([
        ...itemSize,
        {
          size: data,
          qty: 1,
          status: 'unComplete',
          detail: [{
            modalRME: 0,
            kurs: 0,
            ongkir: 0,
            modal: 0,
            hargaJual: 0,
            totalModal: 0,
            itemProfit: 0
          }]
        }
      ])
    } else {
      const arr = itemSize.filter(item => item.size !== data)
      handleChangeSize(arr)
    }
  }

  function isActiveCheckBox (data) {
    return itemSize.some(i => i.size === +data)
  }

  function renderCheckbox (type) {
    const sizeChart = []
    // const check = []

    for (let size = 5; size < 14; size++) {
      sizeChart.push(size)
      sizeChart.push(size + 0.5)
    }

    // if (!_.isEmpty(itemData)) {
    //   for (const fromBody of itemSize) {
    //     check.push(fromBody.size)
    //   }
    // }

    return (
      <>
        <div id='modal-select-multiple' className='itemModal' style={{ background: 'unset' }}>
          {sizeChart.map((data, idx) => (
            <div key={idx} style={{ marginRight: idx < sizeChart.length ? 5 : 0, display: 'flex' }}>
              <input
                name='itemCheckBox' type='checkbox' id={`myCheck-${idx}`} onChange={(e) => getCheckedCheckboxesFor(data, e)}
                checked={isActiveCheckBox(data)}

              />
              <label htmlFor={`myCheck-${idx}`}>{data}</label>
            </div>
          ))}
        </div>
      </>
    )
  }

  function renderOption (type, handleChange, stateValue) {
    let optionRender
    if (type === 'condition') {
      optionRender = itemConditionOption
    } else if (type === 'brand') {
      optionRender = brandOption
    } else if (type === 'type') {
      optionRender = itemTypeOption
    } else if (type === 'gender') {
      optionRender = genderOption
    } else if (type === 'rating') {
      optionRender = ratingOption
    } else {
      optionRender = itemConditionOption
    }

    return (
      <>
        <select
          id='modal-select' className='itemModal' style={{ background: 'unset' }}
          onChange={(e) => handleChange(e.target.value)}
          value={stateValue}
        >
          {optionRender.map((data, idx) => (
            <option value={data.name} key={idx}>{data.name}</option>
          ))}
        </select>
      </>
    )
  }

  function onChangeManageImage (idx, e) {
    const spreadItemImage = [...itemImages]
    const spreadUrl = { ...spreadItemImage[idx] }

    spreadUrl.imageUrl = e.target.value
    spreadItemImage[idx] = spreadUrl
    // spreadUrl = itemImages
    // handleSetImages(spreadItemImage)
    handleSetImages(spreadItemImage)
  }

  function addItemImage (params) {
    let spreadItemImage = [...itemImages]
    spreadItemImage = [...itemImages, { imageUrl: '' }]

    handleSetImages(spreadItemImage)
  }

  function manageLeftContent () {
    return (
      <div
        className='h-full w-full column'
      >
        <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '5%' }}>
          <div id='itemModal' className='font'>Item Name</div>
          <input
            id='m-input'
            value={name}
            onChange={(e) => handleChangeItemName(e.target.value)}
            placeholder='Input Item Name'
            className='itemModal'
          />
        </div>

        <div className='h-full w-full column' style={{ maxWidth: 545, maxHeight: 200 }}>
          <div className='all-inherit flex' style={{ maxHeight: 65 }}>
            <div className='h-full w-full column' style={{ marginTop: '3%', marginRight: '5%' }}>
              <div id='itemModal' className='font'>Item SKU</div>
              <input
                id='m-input'
                value={sku}
                onChange={(e) => handleChangeSku(e.target.value)}
                placeholder='Input Item SKU'
                className='itemModal'
              />
            </div>

            <div className='h-full w-full column' style={{ marginTop: '3%' }}>
              <div id='itemModal' className='font'>Retail Price</div>
              <input
                id='m-input'
                value={retailPrice}
                onChange={(e) => handleChangeRetailPrice(e.target.value)}
                placeholder='Input Retail Price'
                className='itemModal'
              />
            </div>
          </div>

          <div className='all-inherit flex' style={{ maxHeight: 65, marginTop: '8%' }}>
            <div id='conditionItem' className='h-full w-full column' style={{ marginRight: '5%' }}>
              <div id='itemModal' className='font'>Item Condition</div>
              {renderOption('condition', handleChangeCondition, condition)}
            </div>

            <div className='h-full w-full column'>
              <div id='itemModal' className='font'>Colorway</div>
              <input
                id='m-input' placeholder='Input Colorway' className='itemModal'
                value={colorway}
                onChange={(e) => handleChangeColorway(e.target.value)}
              />
            </div>
          </div>

        </div>

        <div className='h-full w-full flex' style={{ maxWidth: 565, maxHeight: 160 }}>
          <div className='all-inherit column' style={{ maxHeight: 147, width: '109%' }}>
            <div className='h-full w-full column' style={{ marginRight: '5%' }}>
              <div id='itemModal' className='font'>Item Rating</div>
              {renderOption('rating', handleChangeRating, rating)}
            </div>

            <div className='h-full w-full column' style={{ marginRight: '5%', marginTop: '10%' }}>
              <div id='itemModal' className='font'>Item Type</div>
              {renderOption('type', handleChangeType, type)}
            </div>
          </div>

          <div className='all-inherit column' style={{ maxHeight: 147, width: '105%' }}>
            <div className='h-full w-full column' style={{ marginRight: '5%' }}>
              <div id='itemModal' className='font'>Gender</div>
              {renderOption('gender', handleChangeGender, gender)}
            </div>

            <div className='h-full w-full column' style={{ marginRight: '5%', marginTop: '10%' }}>
              <div id='itemModal' className='font'>Brand</div>
              {renderOption('brand', handleChangeBrand, brand)}
            </div>
          </div>

          <div className='all-inherit column' style={{ maxHeight: 147, width: '105%' }}>
            <div className='h-full w-full column' style={{ marginRight: '5%' }}>
              <div id='itemModal' className='font'>Item Size</div>
              {renderCheckbox('size')}
            </div>

            <div className='h-full w-full column' style={{ marginRight: '5%', marginTop: '10%' }}>
              <div id='itemModal' className='font'>Release Date</div>
              <div className='column center h-full' key={1} style={{ width: '87%' }}>
                <DayPickerInput
                  format='MM/DD/YYYY'
                  formatDate={formatDate}
                  parseDate={parseDate}
                  onDayChange={(e) => handleChangeDate(moment(e).format('MM/DD/YYYY'))}
                  value={releaseDate}
                  // dayPickerProps={{
                    // disabledDays: { before: new Date() }
                  // }}
                />
              </div>
            </div>
          </div>
        </div>

        <div className='h-full w-full column' style={{ maxWidth: 545 }}>
          <div id='manageItemImageTitleCnt' className='center'>
            <p id='manageItemImageTitle'>
              Manage Item Image
            </p>
          </div>
          <div id='manageItemImage' className='rad-tiny center column'>
            <div id='imageList'>
              <div id='imageListCnt' className='column'>
                <div id='imageListHeader' className='flex'>
                  <div id='imageListSubHdr'>Total</div>
                  <div id='imageListSubHdr'>image Link</div>
                  <div id='imageListSubHdr'>Action</div>
                </div>
              </div>
              <div id='imageMapping'>
                <div id='imageListCnt' className={`column ${itemImages.length > 3 ? 'cnt' : ''}`}>
                  {itemImages.map((data, idx, root) => (
                    <div id='imageListHeader' key={idx} className='flex center' style={{ minHeight: 50 }}>
                      <div id='imageListSubHdr' className='center'>
                        <div>{idx + 1}</div>
                      </div>
                      <div id='imageListSubHdr'>
                        <input id='m-input' placeholder='Input Item Image ' onChange={(e) => onChangeManageImage(idx, e)} value={data.imageUrl} />
                      </div>
                      <div id='imageListSubHdr' className='center'>
                        <div>
                          <ImageIcon className='injectStyle' onClick={() => handleImageModalAction('show', data, true)} />
                        </div>
                        {root.length > 1 &&
                          <div>
                            <DeleteIcon sx={{ color: 'red' }} className='injectStyle' onClick={() => handleImageModalAction('delete', data, true, idx, root)} />
                          </div>}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div id='addImage' onClick={() => addItemImage()}><AddIcon /></div>
          </div>
        </div>
      </div>
    )
  }

  function handleImageModalAction (action, data, type, idx, root) {
    if (action === 'delete') {
      const spreadItemImage = [...itemImages]
      spreadItemImage.splice(idx, 1)

      handleSetImages(spreadItemImage)
    } else {
      handleTriggerImageModal(!showimageModal)
      if (type) {
        handleImageLink(data.imageUrl)
      }
    }
  }

  function changeSizeQty (idx, e) {
    const detailItem = {
      modalRME: '',
      kurs: '',
      ongkir: '',
      modal: '',
      hargaJual: '',
      itemProfit: ''
    }

    const spreadItemSize = [...itemSize]
    let spreadItemIdx = { ...spreadItemSize[idx] }
    spreadItemIdx.qty = e.target.value
    const detail = []
    const statusQty = []
    for (let loop = 0; loop < spreadItemIdx.qty; loop++) {
      // code block to be executed
      if (_.isEmpty(detail[loop]) && !_.isEmpty(itemSize[loop])) {
        detail.push(detailItem)
      }
      statusQty.push('unComplete')
    }
    spreadItemIdx.detail = [...spreadItemIdx.detail, ...detail]
    spreadItemIdx.status = [...statusQty]
    spreadItemSize[idx] = spreadItemIdx
    spreadItemIdx = itemSize

    handleChangeSize(spreadItemSize)
  }

  function handleOnClickSizeDetail (data, idx) {
    handleChangeActiveIndexSize(idx)
    handleChangeActiveIndexSizeDetail(0) // netral
    handleChangeSizeDetailData(data)
  }

  function manageMiddleContent () {
    return (
      <div className='h-full w-full column'>

        <div className='h-full w-full column' id='modalArea'>
          <div id='itemModal' className='font'>Item Description</div>
          <textarea value={itemDescription} className='itemArea' onChange={(e) => handleChangeDescription(e.target.value)} />
        </div>

        <div className='h-full w-full column' id='itemCoreCnt' style={{ maxWidth: 585, maxHeight: 270, padding: '3% 0px 0px', background: 'white', justifyContent: 'center' }}>
          <div id='manageItemImageTitleCnt' className='center'>
            <p id='manageItemImageTitle'>
              Item Size Management
            </p>
          </div>
          <div id='manageItemImage' className='rad-tiny column'>
            <div id='imageListCnt' className='column'>
              <div id='imageListHeader' className='flex w-full center'>
                <div id='imageListSubHdr'>Size</div>
                <div id='imageListSubHdr' style={{ width: '55%' }}>Item Status</div>
                <div id='imageListSubHdr'>Quantity</div>
              </div>
            </div>
            <div id='imageMapping'>
              <div id='imageListCnt' className={`column ${itemSize.length > 3 ? 'scroll' : ''} ${_.isEmpty(itemSize) ? 'center bold' : ''}`}>
                {!_.isEmpty(itemSize)
                  ? itemSize.map((data, idx) => (
                    <div
                      id='imageListHeader' key={idx} className={`flex center item ${activeIndexSize === idx && !_.isEmpty(sizeDetailData) ? 'active' : ''}`}
                      style={{ minHeight: 50, marginTop: 10 }}
                      onClick={() => handleOnClickSizeDetail(data, idx)}
                    >
                      <div id='imageListSubHdr' className='center'>
                        <div>{data.size}</div>
                      </div>
                      <div id='imageListSubHdr' className='center h-full' style={{ width: '55%' }}>
                        {itemStatus(data)}
                      </div>
                      <div id='imageListSubHdr' className='center'>
                        <input
                          type='number'
                          value={data.qty}
                          id='m-input' min='0'
                          style={{ width: '40%', textAlign: 'center' }}
                          onChange={(e) => changeSizeQty(idx, e)}
                        />
                      </div>
                    </div>
                    ))
                  : 'Pilih Size Terlebih Dahulu'}
              </div>
            </div>
          </div>
        </div>

        <div style={{ height: '20%' }}>
          <button style={{ width: '100%', height: '55%', marginTop: '2%', backgroundColor: '#FF9057' }} onClick={() => submitItem()}>
            {actionClick}
          </button>
        </div>

      </div>
    )
  }

  function itemStatus (data) {
    // const status = []
    // for (let index = 0; index < data.qty; index++) {
    //   status.push({ status: data.status })
    // }

    return (
      data.detail.map((data, idx) => (
        <div key={idx} style={{ borderRadius: '100%', backgroundColor: data.status === 'complete' ? 'green' : '', height: 10, width: 10, marginRight: 4, border: '1px grey solid' }} />
      ))
    )
  }

  function manageRightContent () {
    const calculateModal = 0
    const calculateTotalModal = 0
    const calculateProfit = 0

    if (!_.isEmpty(itemSize) && !_.isEmpty(sizeDetailData)) {
      // const modalRME = parseInt(itemSize[activeIndexSize].detail[activeIndexSizeDetail].modalRME)
      // const kurs = parseInt(itemSize[activeIndexSize].detail[activeIndexSizeDetail].kurs)
      // const ongkir = parseInt(itemSize[activeIndexSize].detail[activeIndexSizeDetail].ongkir)
      // const hargaJual = parseInt(itemSize[activeIndexSize].detail[activeIndexSizeDetail].hargaJual)

      // calculateModal = (modalRME * kurs) + ongkir
      // calculateTotalModal = ((calculateModal * 0.06) + calculateModal)
      // calculateProfit = hargaJual - calculateTotalModal
    }

    return (
      <div className='h-full w-full column center' style={{ backgroundColor: '#F0F0F1', height: '95%', marginLeft: '5%', borderRadius: 20 }}>
        {!_.isEmpty(itemSize) && !_.isEmpty(sizeDetailData)
          ? <>
            <div className='h-tiny w-half center' style={{ background: 'white', borderRadius: 10 }}>
              <div id='itemModal' className='font'>
                Internal Data Management
              </div>
            </div>
            <div className='h-full w-full flex'>
              <div className='h-full w-full column' style={{ marginLeft: '5%', marginRight: '2%' }}>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '10%' }}>
                  <div id='itemModal' className='font'>Modal RME</div>
                  <input
                    id='m-input' placeholder='Input Modal RME'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].modalRME || 0}
                    name='modalRME'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                    className='itemModal'
                  />
                </div>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '7%' }}>
                  <div id='itemModal' className='font'>Kurs</div>
                  <input
                    id='m-input' placeholder='Input Kurs'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].kurs || 0}
                    name='kurs'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                    className='itemModal'
                  />
                </div>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '7%' }}>
                  <div id='itemModal' className='font'>Ongkir</div>
                  <input
                    id='m-input' placeholder='Input Ongkir' className='itemModal'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].ongkir || 0}
                    name='ongkir'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                  />
                </div>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '7%' }}>
                  <div id='itemModal' className='font'>Modal</div>
                  <input
                    disabled
                    id='m-input' placeholder='Input Modal' className='itemModal'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].modal || 0}
                    name='modal'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                  />
                </div>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '7%' }}>
                  <div id='itemModal' className='font'>Total Modal</div>
                  <input
                    disabled
                    id='m-input' placeholder='Input Harga Jual' className='itemModal'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].totalModal || 0}
                    name='totalModal'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                  />
                </div>
                <div className='h-full w-full column' style={{ maxHeight: 65, height: '29%', marginTop: '7%' }}>
                  <div id='itemModal' className='font'>Harga Jual</div>
                  <input
                    id='m-input' placeholder='Input Item Profit' className='itemModal'
                    value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].hargaJual || 0}
                    name='hargaJual'
                    onChange={(e) => handleChangeRightContent(e.target.value, e.target.name)}
                  />
                </div>
              </div>

              <div className='h-full w-half column'>
                <div
                  id='manageItemImage'
                  className='rad-tiny column'
                  style={{
                    height: '88%',
                    background: 'white',
                    marginTop: '23%',
                    borderRadius: 8,
                    alignItems: 'center'
                  }}
                >
                  <div
                    style={{
                      height: '5%',
                      width: '70%'
                    }}
                    className='center line-hr'
                  >
                    <div id='itemModal' className='font'>{`Size ${!_.isEmpty(sizeDetailData) ? sizeDetailData.size : '-'}`}</div>
                  </div>
                  {/* {!_.isEmpty(itemSize.detail)
                ? itemSize.detail.map((data, idx) => (
                  <div id='sizeManagement' key={idx}>
                    <button className={`${idx === 0 ? 'active' : ''}`}>Item {idx + 1}</button>
                  </div>
                  ))
                : null} */}
                  {!_.isEmpty(itemSize) && !_.isEmpty(sizeDetailData)
                    ? itemSize[activeIndexSize].detail.map((data, idx) => (
                      <div id='sizeManagement' key={idx}>
                        <button className={`${activeIndexSizeDetail === idx ? 'active' : ''}`} onClick={() => handleChangeActiveIndexSizeDetail(idx)}>Item {idx + 1}</button>
                      </div>
                      ))
                    : null}
                </div>
              </div>
            </div>

            <div className='h-tiny w-full' style={{ height: '25%' }}>
              <div id='manageItemData' className='h-full w-full column center' style={{ marginTop: '3%', justifyContent: 'start' }}>
                <div id='itemModal' className='font'>Profif</div>
                <input
                  id='m-input'
                  value={itemSize[activeIndexSize].detail[activeIndexSizeDetail].itemProfit || 0} placeholder='Input Modal RME' className='itemModal' disabled
                />
              </div>
            </div>
            </>
          : <h2>
            Pilih Size Terlebih Dahulu
            </h2>}
      </div>
    )
  }

  function handleChangeRightContent (e, name) {
    const spreadItemSize = [...itemSize]
    const spreadItemIdx = { ...spreadItemSize[activeIndexSize] }
    const spreadItemDetail = [...spreadItemIdx.detail]
    const spreadItemActiveIdx = { ...spreadItemDetail[activeIndexSizeDetail] }

    spreadItemActiveIdx[name] = parseInt(e)
    spreadItemActiveIdx.modal = (spreadItemActiveIdx.modalRME * spreadItemActiveIdx.kurs) + spreadItemActiveIdx.ongkir
    spreadItemActiveIdx.totalModal = ((spreadItemActiveIdx.modal * 0.06) + spreadItemActiveIdx.modal)
    spreadItemActiveIdx.itemProfit = spreadItemActiveIdx.hargaJual - spreadItemActiveIdx.totalModal

    if (spreadItemActiveIdx[name] !== null && !isNaN(spreadItemActiveIdx[name])) {
      spreadItemActiveIdx.status = 'complete'
    } else {
      spreadItemActiveIdx.status = 'uncomplete'
    }

    spreadItemDetail[activeIndexSizeDetail] = spreadItemActiveIdx
    spreadItemIdx.detail = spreadItemDetail
    spreadItemSize[activeIndexSize] = spreadItemIdx
    handleChangeSize(spreadItemSize)
  }

  function submitItem () {
    const data = {
      itemName: name,
      sku: sku,
      retailPrice: retailPrice,
      itemCondition: condition,
      colorway: colorway,
      itemRating: rating,
      gender: gender,
      itemSize: itemSize,
      itemType: type,
      brand: brand,
      releaseDate: releaseDate,
      itemImage: itemImages,
      itemDescription: itemDescription
    }

    const itemStatus = checkItemStatus(data)
    if (itemStatus) {
      toast.error('Lengkapi Data terlebih Dahulu')
    } else {
      if (actionClick === 'Submit') {
        addNewItems(data, null, null, setSuccess, `Create Item ${data.itemName}`, getAllItems)
        triggerModal()
      } else {
        updateItems(data, null, null, setSuccess, 'Edit Campaign', itemId, getAllItems)
        triggerModal()
      }
    }
  }

  function checkItemStatus (test) {
    let validateIs = false

    for (const property in test) {
      validateIs = _.isEmpty(test[property]) && true
    }

    return validateIs
  }

  return (
    <>
      <ImageModal showimageModal={showimageModal} imageLink={imageLink} handleTriggerImageModal={handleTriggerImageModal} />
      <div id='modal-header' className='column center' style={{ height: '8%' }}>
        <div id='modal-title' className='w-full center flex' style={{ padding: 'unset' }}>ADD ITEM</div>
        <hr id='m-hr' />
      </div>

      <div id='modal-body-item' className='w-full flex'>
        <div className='h-full w-full flex center' id='manage-left' style={{ paddingLeft: '2%' }}>
          {manageLeftContent()}
        </div>
        <div className='h-full w-full flex center' id='manage-middle'>
          {manageMiddleContent()}
        </div>
        <div className='h-full w-full flex center' id='manage-right' style={{ paddingRight: '2%' }}>
          {manageRightContent()}
        </div>
      </div>
    </>
  )
}
