import React, { Fragment, useEffect, useState } from 'react'
import _ from 'lodash'

export default function ImageModal (props) {
  //   const [imageSrc, changeImageSrc] = useState()

  useEffect(() => {
    // c
  }, [])

  // handleTriggerImageModal={handleTriggerImageModal}

  return (
    <>
      {props.showimageModal &&
        <div id='imageModal' onClick={() => props.handleTriggerImageModal()}>
          <img
            id='itemImagePreview'
            alt='Girl in a jacket'
            width='550'
            height='550'
            src={`https://drive.google.com/uc?export=view&id=${props.imageLink}`}
          />
        </div>}
    </>
  )
}
