import React from 'react'
import TopProduct from '../../global/TopProduct'
import AddIcon from '@mui/icons-material/Add'

import Button from '../../Components/Home/Campaigns/functions/Button'
import Magazine from '../../global/Magazine'
export default function ModalContent ({ toggleModal, handleToogleModal, contentData, onSubmitTrigger, isFromDisplay, query }) {
  function renderFooter () {
    return (
      <Button
        value={`${isFromDisplay ? 'Update' : 'Add'} Top Product`}
        handleOnClick={onSubmitTrigger}
        type='submit-button-primary' size='l' fontSize='l' name='campaign-header' fontWeight='medium' height='unset' icon={<AddIcon className='padding-right-xs' sx={{ fontSize: 30 }} />}
      />
    )
  }

  if (toggleModal) {
    return (
      <div className='flex modal-content center' id='modal'>
        <div className='h-full w-full absolute' onClick={() => handleToogleModal()} />
        <div className={`column modal absolute ${query.charAt(0).toLowerCase() + query.slice(1)}-container`}>

          {query === 'TopProduct' && <TopProduct contentData={contentData} />}
          {query === 'Magazine' && <Magazine contentData={contentData} />}

          <div className='topProduct-container-footer'>
            {renderFooter()}
          </div>
        </div>
      </div>
    )
  } else {
    return null
  }
}
