import React, { useContext } from 'react'
import _ from 'lodash'
import { StateContext } from '../StateContext'
import CampaignModal from './Components/CampaignModal'
import ItemModal from './Components/ItemModal'

export default function Modal (props) {
  const [context] = useContext(StateContext)

  const { displayModal, navigation, triggerModal, triggerFromBody, fromBody } = context
  // isActiveUrl === true kalau match dgn url yg sdh ada

  function handleOnClick () {
    triggerModal(false)
    triggerFromBody(false)
  }

  if (displayModal) {
    return (
      <div className='flex h-full w-full absolute center' id='modal'>
        <div className='h-full w-full absolute' style={{ zIndex: 1 }} onClick={() => handleOnClick()} />
        <div className={`column center h-medium w-ex-half modal absolute center ${navigation === 'campaign' ? '' : 'items-modal'}`} style={{ zIndex: 2 }}>
          {navigation === 'campaign'
            ? <CampaignModal />
            : <ItemModal />}
        </div>
      </div>
    )
  } else {
    return null
  }
}
