import React, { createContext, useState, useEffect } from 'react'

const StateContext = createContext([{}, () => {}])

const StateProvider = (props) => {
  const [attbContext, setattbContext] = useState({})

  useEffect(() => {
    setattbContext(props)
  }, [props])

  return (
    <StateContext.Provider value={[attbContext, setattbContext]}>
      {props.children}
    </StateContext.Provider>
  )
}

export { StateProvider, StateContext }
