import React, { Fragment, useEffect, useState } from 'react'
// import logo from './logo.svg'
import './App.css'
// import './App.css'
// import mainscss
import '../src/Components/Home/Campaigns/styles/main.scss'

import Header from './Components/Home/Header'
import Body from './Components/Home/Body'
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom'

import 'react-day-picker/lib/style.css'
// import 'react-day-picker/lib/style.css'
// import { toast, ToastContainer } from 'react-toastify'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import Modal from './Components/Modal/Modal'
import Home from './Components/Home/Home'
import moment from 'moment'
import config from './config'
import { addNewCampaigns, fetchGetAllCampaigns, deleteCampaignsData, updateCampaignsData, fetchGetAllItems, deleteItemData, fetchGetAllContent, fetchGetCampaignsByName, fetchItemsByValue, getActiveUserData, getUserData } from './Components/Services/FetchRequestGroup'

import { StateProvider } from './Components/StateContext'
import _ from 'lodash'
import DetailCampaign from './Components/Home/Campaigns/DetailCampaign'
import Content from './Components/Home/Campaigns/Content'
import DetailContent from './Components/Home/Campaigns/DetailContent'
import LoginPage from './global/LoginPage'

export default function App (props) {
  const [displayModal, triggerModal] = useState(false)

  const [fromBody, triggerFromBody] = useState(false)

  const [inputField] = useState([
    { name: 'Name', value: 'campaignName', id: 'setCampaignName' },
    { name: 'Title', value: 'title', id: 'setTitle' },
    { name: 'Url', value: 'urlKey', id: 'setUrlKey' }
  ])
  const [dateField] = useState(
    [
      { name: 'Start Date', value: 'startDate' },
      { name: 'End Date', value: 'endDate' }
    ]
  )
  const [startDate, setStartDate] = useState(
    moment(new Date()).format('MM/DD/YYYY')
  )
  const [endDate, setEndDate] = useState(
    moment(new Date()).format('MM/DD/YYYY')
  )
  const [campaignName, setCampaignName] = useState('')
  const [title, setTitle] = useState('')
  const [urlKey, setUrlKey] = useState('')
  const [success, setSuccess] = useState({ success: false, data: {} })
  const [campaigns, setCampaigns] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [actionClick, changeActionClick] = useState('Add')
  const [campaignId, setCampaignId] = useState('')
  const [isActiveUrl, checkIsActiveUrl] = useState(false)
  // items
  const [items, setItems] = useState([])
  const [url, changeUrl] = useState(false)
  // location url
  const [navigation, navigateTo] = useState('campaign')
  // content
  const [content, setContent] = useState([])

  const [keyword, setKeyword] = useState(null)

  const [isErr, setIsErr] = useState(false)
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: App.js ~ line 73 ~ isErr', isErr)

  const [isGettingUser, setIsGettingUser] = useState(true)

  // user
  const [userData, setUserData] = useState(false)
  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: App.js ~ line 76 ~ userData', userData)
  // const [userData3, setUserData3] = useState(false)

  function GetMainData () {
    console.log('masuk main data')

    console.log('masuk fetch2')
    let activeUrl = { ...config }
    if (!_.isEmpty(config)) {
      activeUrl = activeUrl.baseUrl
    }
    if (window.location.href === activeUrl) {
      getAllCampaigns()
      navigateTo('campaign')
    } else if (window.location.href === activeUrl + 'item') {
      getAllItems()
      navigateTo('item')
    } else if (window.location.href === activeUrl + 'content') {
      getAllContent()
      navigateTo('content')
    }
  }

  // function getIsActiveUser (params) {
  //   console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: App.js ~ line 109 ~ params', params)
  //   getActiveUserData(setIsLoading, null, setUserData, null, params)
  // }

  // useEffect(() => {
  //   console.log('masuk fetch1')
  //   // userData.data && GetMainData()
  //   userData.data.accessToken &&
  // }, [userData.data.accessToken])

  useEffect(() => {
    console.log('masuk fetch2')

    if (!_.isEmpty(userData)) {
      if (!_.isEmpty(userData?.data[0])) {
        userData?.data[0].name && GetMainData()
      } else {
        if (!_.isEmpty(userData?.data?.accessToken)) {
          localStorage.setItem('userToken', userData.data.accessToken)
        }
        getUserDataCallBack()
      }
    }
  }, [userData])

  function getUserDataCallBack () {
    let userToken
    if (userData?.data?.accessToken) userToken = userData.data.accessToken
    if (!userData?.data?.accessToken) userToken = localStorage.getItem('userToken')
    console.log('trace 123', userToken)
    getUserData(setIsLoading, setIsErr, setUserData, null, userToken)
  }

  console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: App.js ~ line 116 ~ userData?.data?.nama', userData?.data)

  useEffect(() => {
    console.log('masuk 109', localStorage.getItem('userToken'))
    getUserDataCallBack()
    // if (!_.isEmpty(localStorage.getItem('userToken'))) {
    //   setIsGettingUser(true)
    // }
    // getIsActiveUser(localStorage.getItem('userToken'))
  }, [])

  useEffect(() => {
    // setItems()x
  }, [items])

  useEffect(() => {
    if (success.success) {
      setSuccess({ success: false, data: {} })
      fetchGetAllCampaigns(setCampaigns, setIsLoading, null)
      initCampaignForm()
    }
  }, [success])

  useEffect(() => {
    if (url) {
      GetMainData()
    }
    changeUrl(false)
  }, [url])

  function handleChangeUrl (activeUrl) {
    changeUrl(!url)
  }

  function initCampaignForm () {
    setTitle('')
    setUrlKey('')
    setStartDate(moment(new Date()).format('MM/DD/YYYY'))
    setEndDate(moment(new Date()).format('MM/DD/YYYY'))
    setCampaignName('')
    changeActionClick('Add')
    setCampaignId('')
  }

  function getAllCampaigns (offsetProps) {
    fetchGetAllCampaigns(setCampaigns, setIsLoading, null)
  }

  function handleTriggerModal () {
    triggerModal(!displayModal)
  }

  function handleSubmitCampaign () {
    const newCampaign = {
      campaignData: [],
      campaignName: campaignName,
      title: title,
      urlKey: urlKey,
      startDate: startDate,
      endDate: endDate
    }
    let dataErr = ''

    if (newCampaign.campaignName === '') {
      dataErr = 'Campaign Name'
    } else if (newCampaign.title === '') {
      dataErr = 'Campaign Title'
    } else if (newCampaign.urlKey === '' || isActiveUrl) {
      dataErr = 'Campaign Url'
    }

    if (!_.isEmpty(dataErr)) {
      let message = `Lengkapi ${dataErr}`
      if (isActiveUrl) {
        message = `${dataErr} sudah pernah digunakan`
      }
      toast.error(message, { className: 'font-size-m bold' })
    } else {
      if (actionClick === 'Add') {
        addNewCampaigns(newCampaign, null, null, setSuccess, 'Create Campaign')
      } else {
        updateCampaignsData(newCampaign, null, null, setSuccess, 'Edit Campaign', campaignId)
      }
      handleTriggerModal()
    }
  }

  function onHandleDeleteCampaign (id, type) {
    let text = 'Do you want Delete this Campaign?'
    if (type === 'item') {
      text = 'Do you want Delete this Item?'

      if (window.confirm(text) === true) {
        deleteItemData(id, null, null, setSuccess, 'Delete Campaign', getAllItems)
      } else {
        toast.error('Delete campaign dibatalkan')
      }
    } else {
      if (window.confirm(text) === true) {
        deleteCampaignsData(id, null, null, setSuccess, 'Delete Campaign')
      } else {
        toast.error('Delete campaign dibatalkan')
      }
    }
  }

  function editCampaigns (id, data) {
    if (campaignName === '') {
      setCampaignName(data.campaignName)
    }
    if (title === '') {
      setTitle(data.title)
    }
    if (urlKey === '') {
      setUrlKey(data.urlKey)
    }
    setCampaignId(id)
    changeActionClick('Update')
    handleTriggerModal()
  }

  // items

  function getAllItems () {
    fetchGetAllItems(setItems, setIsLoading, null)
  }

  // content
  function getAllContent () {
    fetchGetAllContent(setContent, setIsLoading, null)
  }

  const [itemId, setItemId] = useState('')
  const [itemData, setitemData] = useState({})

  function editItems (id, data) {
    setItemId(id)
    setitemData(data)
    handleTriggerModal()
  }

  function filterData () {
    if (keyword !== '') {
      setKeyword(() => '',
        navigation === 'campaign' && fetchGetCampaignsByName(setCampaigns, setIsLoading, null, keyword),
        navigation === 'item' && fetchItemsByValue(keyword, setItems, setIsLoading, null)
      )
    } else {
      if (navigation === 'campaign') {
        getAllCampaigns()
      } else {
        getAllItems()
      }
    }
  }

  return (
    <StateProvider
      setUserData={setUserData}
      userData={userData}
      displayModal={displayModal}
      campaigns={campaigns}
      isLoading={isLoading}
      setIsLoading={setIsLoading}
      // State
      campaignName={campaignName}
      urlKey={urlKey}
      title={title}
      inputField={inputField}
      dateField={dateField}
      startDate={startDate}
      endDate={endDate}
      actionClick={actionClick}
      // check active url
      isActiveUrl={isActiveUrl}
      checkIsActiveUrl={checkIsActiveUrl}
      // set Handle
      handleChangeUrl={handleChangeUrl}
      triggerModal={handleTriggerModal}
      onHandleDeleteCampaign={onHandleDeleteCampaign}
      editCampaigns={editCampaigns}
      handleSubmitCampaign={handleSubmitCampaign}
      setUrlKey={setUrlKey}
      setTitle={setTitle}
      setCampaignName={setCampaignName}
      config={config}
      // items
      items={items}
      navigation={navigation}
      getAllItems={getAllItems}
      editItems={editItems} // edit
      itemId={itemId}
      itemData={itemData}
      // modal
      // handleTriggerModal={handleTriggerModal}
      // fromBody
      triggerFromBody={triggerFromBody}
      fromBody={fromBody}
      // content
      content={content}
    >

      <div className='App'>
        {console.log(' 🚀 ~ %c (っ◔◡◔)っ', 'background:black; color:white;', ' ~ file: App.js ~ line 345 ~ {!_.isEmpty(userData.data) ', !_.isEmpty(userData.data))}
        <BrowserRouter>
          <Routes>
            <Route
              path='/'
              element={
                <>
                  {!_.isEmpty(userData.data)
                    ? <>
                      <ToastContainer
                        autoClose={2000}
                      />
                      <Modal />
                      <Home campaigns={campaigns} isLoading={isLoading} setKeyword={setKeyword} filterData={filterData} keyword={keyword} navigation={navigation} />
                    </>
                    : (isErr === 'Forbidden' || _.isEmpty(localStorage.getItem('userToken'))) && <Navigate to='/login' />}
                </>
              }
            />

            <Route
              path='item'
              element={
                <>
                  <ToastContainer />
                  <Modal />
                  <Home campaigns={campaigns} isLoading={isLoading} type='item' setKeyword={setKeyword} filterData={filterData} keyword={keyword} navigation={navigation} />
                </>
            }
            />
            <Route
              path='/campaigns/:id'
              element={
                <>
                  <ToastContainer
                    position='top-right'
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                  />
                  <Modal />
                  <DetailCampaign navigateTo={navigateTo} campaigns={campaigns} isLoading={isLoading} userData={userData} />
                </>
              }
            />

            <Route
              path='/content'
              element={
                <>
                  <ToastContainer
                    position='top-right'
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                  />
                  <Modal />
                  <Home campaigns={campaigns} isLoading={isLoading} type='content' navigation={navigation} />
                </>
              }
            />

            <Route
              path='/content/:id'
              element={
                <>
                  <ToastContainer
                    position='top-right'
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    draggable
                    pauseOnHover
                  />
                  <Modal />
                  <DetailContent navigateTo={navigateTo} campaigns={content} isLoading={isLoading} userData={userData} />
                </>
              }
            />

            <Route
              path='/login'
              element={
                <>
                  {!_.isEmpty(userData.data)
                    ? <Navigate to='/' />
                    : <LoginPage setUserData={setUserData} getUserDataCallBack={getUserDataCallBack} />}
                </>
              }
            />
          </Routes>

        </BrowserRouter>
      </div>
    </StateProvider>
  )
}
