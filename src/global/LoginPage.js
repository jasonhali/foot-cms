import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import CustomLazyLoadImage from '../Components/Home/Campaigns/functions/CustomlazyLoadImage'
import { getAllContentData, getUserData, getUserDataJWT } from '../Components/Services/FetchRequestGroup'

export default function LoginPage ({ setUserData, getUserDataCallBack }) {
  const [loadingImage, isLoadingImage] = useState(false)

  const [userName, setUserName] = useState(null)
  const [password, setPassword] = useState(null)

  function renderHeaderLogo (params) {
    return (
      <CustomLazyLoadImage
        loadingImage={loadingImage}
        isLoadingImage={isLoadingImage}
        prevIsLoading
        image='https://res.cloudinary.com/donu8hbzs/image/upload/v1656770446/Content%20Management%20System/Brand%20Logo/FeetFoot/FeetFoot_aat2bx.png'
      />
    )
  }

  // const { isLoading, error, data, isFetching } = useQuery('repoData', () =>
  //   axios.get(
  //     'https://api.github.com/repos/tannerlinsley/react-query'
  //   ).then((res) => res.data)
  // )

  const [isLoading, setIsLoading] = useState(false)
  const [isErr, setIsErr] = useState(false)

  function loginClicked (params) {
    const query = {
      name: userName,
      password: password
    }
    getUserDataJWT(setIsLoading, setIsErr, setUserData, null, query)
  }

  function renderBody (params) {
    return (
      <div className='login-page margin-top-xxl'>
        <div color='light' className='login-page-header'>
          Sign into FeetFoot
        </div>
        <div className='kpWeuB center'>
          <label font-size='0' className='login-page-label'>
            Username
          </label>

          <input
            type='text' placeholder='Username' autocomplete='off' color='text.onLight'
            className='login-page-input'
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className='kpWeuB center'>
          <label font-size='0' className='login-page-label'>
            Password
          </label>

          <input
            type='password' placeholder='Password' autocomplete='off' color='text.onLight' className='login-page-input'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onKeyUp={(e) => e.keyCode === 13 && setPassword(() => e.target.value, loginClicked())}
          />
        </div>

        <div className='kpWeuB center'>

          <button kind='primary' type='submit' className='login-page-submit' onClick={() => loginClicked()}>LOGIN</button>
        </div>
      </div>
    )
  }
  return (
    <div className='login-container-body'>
      {renderHeaderLogo()}
      {renderBody()}
    </div>
  )
}
