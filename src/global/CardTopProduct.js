import React, { Fragment, useContext, useEffect, useState } from 'react'
import { StateContext } from '../Components/StateContext'

// import CustomLazyLoadImage from '../../../functions/CustomlazyLoadImage'
import { Swiper, SwiperSlide } from 'swiper/react'
import DeleteIcon from '@mui/icons-material/Delete'

import 'swiper/css'
import { Navigation } from 'swiper'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import CustomLazyLoadImage from '../Components/Home/Campaigns/functions/CustomlazyLoadImage'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import Button from '../Components/Home/Campaigns/functions/Button'
import _ from 'lodash'

import SwiperCore, { Autoplay } from 'swiper/core'
import Skeleton from 'react-loading-skeleton'
import DummyDiv from '../Components/Home/Campaigns/functions/DummyDiv'
import Input from '../Components/Home/Campaigns/functions/Input'
import { fetchItemsByValue } from '../Components/Services/FetchRequestGroup'
import { LazyLoadImage } from 'react-lazy-load-image-component'

SwiperCore.use([Autoplay])

export default function CardTopProduct ({ cardData, setProduct, idx, setSelectedIndex, idxSKU, idxProduct, setSelectedIndexProduct, clearSKUSection, setClearSKU, setActivePreviewIndex, previewIndex }) {
  const [brandImage, setBrandImage] = useState(null)

  useEffect(() => {
    if (!_.isEmpty(cardData)) {
      if (cardData.brand === 'Nike') {
        setBrandImage('https://res.cloudinary.com/donu8hbzs/image/upload/v1654781564/Content%20Management%20System/Brand%20Logo/Air%20Jordan/AirJordan.png')
      }
    }
  }, [cardData])

  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingItem, setIsLoadingItem] = useState(false)
  const currency = '5300000'

  const handleChangeInput = (e) => {
    setActivePreviewIndex(idx)

    if (e.keyCode === 13 && !_.isEmpty(e.target.value)) {
      // DD1391100
      fetchItemsByValue(e.target.value, setProduct, setIsLoadingItem, null)
    }
  }

  const handleClickClearSKU = () => {
    if (!_.isEmpty(cardData)) {
      setActivePreviewIndex(() => idx,
        setClearSKU(true)
      )
    }
  }

  // const rupiah = () => {
  //   return new Intl.NumberFormat('id-ID', {
  //     style: 'currency',
  //     currency: 'IDR'
  //   }).format(currency)
  // }

  function renderBody () {
    return (
      <div className='card-topProduct-body number-topProduct'>
        <h4>{idx + 1}</h4>
      </div>
    )
  }

  const renderBrandLogo = (cardData) => {
    let imageBranbrandImage

    if (cardData.brand === 'Nike') {
      imageBranbrandImage = 'https://res.cloudinary.com/donu8hbzs/image/upload/v1654781564/Content%20Management%20System/Brand%20Logo/Air%20Jordan/AirJordan.png'
    }
    return (
      <CustomLazyLoadImage
        image={imageBranbrandImage}
        className='topProduct-image'
      />
    )
  }

  function renderBodyTP (idxSKU, idxProduct) {
    if (_.isEmpty(cardData)) {
      const params = { idxSKU, idxProduct }
      return (
        <>
          <p id='cardTitle' className='margin-top-xs'>
            <Input additionalParams={params} type='actionType' placeholder='input SKU' handleOnChange={handleChangeInput} />
          </p>
        </>
      )
    } else {
      return (
        <>
          <div className='cardTopProduct-description'>
            <p id='cardTitle'>
              {cardData.itemName}
            </p>
            <p id='cardPrice'>
              {cardData.colorway}
            </p>
          </div>
          <div className='cardTopProduct-logo'>
            {renderBrandLogo(cardData)}
          </div>
          <div className='cardTopProduct-image'>
            <CustomLazyLoadImage
              image={cardData.itemImage[0].imageUrl}
              className='cardTopProduct-image-pdp'
            />
          </div>
        </>
      )
    }
  }

  return (
    <>
      <div className={`cardProductTopProduct ${(idxSKU + 1 === 1 || idxSKU + 1 === 6 ? '' : 'margin-top-xxl')} ${!_.isEmpty(cardData) ? 'sku-filled' : ''}`} id='cardProductTopProduct'>
        {!_.isEmpty(cardData) &&
          <div className='clearSku'>
            <div onClick={() => handleClickClearSKU(idxSKU, idxProduct)}>
              <DeleteIcon sx={{ color: '#FEFEFC', fontSize: 40 }} />
            </div>
          </div>}

        {renderBody()}

        <div className='hr-line' />

        <div className='cardProductTopProduct-footer'>
          {renderBodyTP(idxSKU, idxProduct)}
        </div>

      </div>
    </>
  )
}
