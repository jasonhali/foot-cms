import _ from 'lodash'
import React, { Fragment, useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import Input from '../Components/Home/Campaigns/functions/Input'
import { StateContext } from '../Components/StateContext'
import CardTopProduct from './CardTopProduct'

export default function TopProduct ({ toggleModal, contentData }) {
  const [context] = useContext(StateContext)
  const { addContentQuery, setAddContentQuery } = context

  function changeInputValue (e, type) {
    const manipulateQuery = { ...addContentQuery }
    manipulateQuery[type] = e.target.value

    setAddContentQuery(manipulateQuery)
  }

  function renderHeader (params) {
    return (
      <div className='topProduct-container-header-urlKey'>
        <Input
          type='transparent' placeholder='Input Top Product UrlKey'
          value={addContentQuery?.urlKey || ''}
          // handleOnKeyPress={handleChangeImageUrl}
          handleOnChange={changeInputValue}
          additionalParams='urlKey'
        />
      </div>
    )
  }

  const [product, setProduct] = useState([])

  const [clearSKU, setClearSKU] = useState(false)
  const [activePreviewIndex, setActivePreviewIndex] = useState(0)

  useEffect(() => {
    if (clearSKU) {
      triggerUpdateWhenAction({}, 'Clear SKU')
    }
  }, [clearSKU])

  useEffect(() => {
    if (!_.isEmpty(product)) {
      triggerUpdateWhenAction(product[0], 'Insert SKU')
    }
  }, [product])

  function triggerUpdateWhenAction (payload, message) {
    const { addContentQuery, setAddContentQuery } = context
    const manipulateQuery = { ...addContentQuery }

    const manipulateQueryProduct = [...manipulateQuery.product]

    manipulateQueryProduct[activePreviewIndex] = payload

    manipulateQuery.product = manipulateQueryProduct

    setAddContentQuery(() => manipulateQuery,
      toast.success(`${message} sucess`, { className: 'font-size-m bold text-center' }),
      setClearSKU(false)
    )
  }

  function renderBody (params) {
    const { addContentQuery } = context

    return (
      <>
        <div className='topProduct-container-body-title'>
          <Input
            type='transparent' placeholder='Input Top Product UrlKey'
            handleOnChange={changeInputValue}
            additionalParams='title'
            value={addContentQuery?.title || ''}
          />
        </div>
        <div className='topProduct-container-body-body'>
          {addContentQuery.product.map((data, idx) => (
            <Fragment key={idx}>
              <CardTopProduct
                type='celebrity-collection'
                idx={idx}
                setClearSKU={setClearSKU}
                cardData={data}
                setProduct={setProduct}
                setActivePreviewIndex={setActivePreviewIndex}
              />
            </Fragment>
          ))}
        </div>
      </>
    )
  }
  return (
    <>
      <div className='topProduct-container-header center'>
        {renderHeader()}
      </div>

      <div className='topProduct-container-body'>
        {renderBody()}
      </div>
    </>
  )
}
