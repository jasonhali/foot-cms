import _, { isEmpty } from 'lodash'
import React, { Fragment, useContext, useEffect, useState } from 'react'
import Slider from 'react-slick'
import { toast } from 'react-toastify'
import Button from '../Components/Home/Campaigns/functions/Button'
import Input from '../Components/Home/Campaigns/functions/Input'
import { StateContext } from '../Components/StateContext'
import AddIcon from '@mui/icons-material/Add'
import CustomLazyLoadImage from '../Components/Home/Campaigns/functions/CustomlazyLoadImage'
import DeleteIcon from '@mui/icons-material/Delete'
import JoditEditor from 'jodit-react'
import config from '../config'

export default function Magazine ({ toggleModal, contentData }) {
  const [context] = useContext(StateContext)
  const { addContentQuery, setAddContentQuery, isFromDisplay } = context

  const [content, setContent] = useState(null)

  useEffect(() => {
    if (isFromDisplay) {
      setContent(addContentQuery.description)
    }
  }, [isFromDisplay !== false])

  useEffect(() => {
    if (content !== '') {
      changeInputValue(content, null, 'description')
    }
  }, [content])

  function changeInputValue (e, idx, type) {
    let value
    if (type === 'delete' || type === 'description') value = e
    if (type !== 'delete' && type !== 'description') value = e.target.value

    const manipulateQuery = { ...addContentQuery }

    if (type !== 'title' && type !== 'description' && type !== 'urlKey') {
      const manipulateQueryProduct = [...manipulateQuery.product]
      const manipulateQueryIndex = { ...manipulateQueryProduct[idx] }

      manipulateQueryIndex.image_url = value
      manipulateQueryProduct[idx] = manipulateQueryIndex
      manipulateQuery.product = manipulateQueryProduct
    } else if (!isEmpty(type)) {
      if (type === 'description') value = content

      manipulateQuery[type] = value
    }

    if (type === 'image') {
      if ((e.keyCode === 13 && !_.isEmpty(value)) || type === 'delete') {
        setAddContentQuery(manipulateQuery)
      }
    } else {
      setAddContentQuery(manipulateQuery)
    }
  }

  function renderHeader (params) {
    return (
      <div className='magazine-container-header-urlKey'>
        <Input
          type='transparent'
          placeholder='Input Magazine UrlKey'
          additionalSecondParams='urlKey'
          value={addContentQuery?.urlKey || ''}
          handleOnChange={changeInputValue}
          additionalParams='urlKey'
        />
      </div>
    )
  }

  const magazineBody = (data, idx, arr) => {
    if (arr.length !== idx + 1) return displayMagazine(data, idx, arr)
    if (arr.length === idx + 1) return addNewMagazine(arr)
  }

  function addNewMagazinePage (arr) {
    const payload = {
      image_url: ''
    }

    const manipulateQuery = { ...addContentQuery }
    let manipulateQueryProduct = [...manipulateQuery.product]
    manipulateQueryProduct = [...manipulateQueryProduct, payload]

    const fromIndex = manipulateQueryProduct.findIndex((object) =>
      object.type === 'increment'
    )
    const toIndex = arr.length

    const element = manipulateQueryProduct.splice(fromIndex, 1)[0]

    manipulateQueryProduct.splice(toIndex, 0, element)

    manipulateQuery.product = manipulateQueryProduct
    setAddContentQuery(manipulateQuery)
    // const manipulateQueryIndex = { ...manipulateQueryProduct[idx] }
  }

  function addNewMagazine (arr) {
    return (
      <Button
        // value='Add'
        type='transparent'
        additionalClass='increment-button'
          // iconAdditionalClass='margin-top-xs'
        size='s'
        fontSize='s'
        name='campaign-header'
        fontWeight='bold'
        height='unset'
        width=''
        handleOnClick={() => addNewMagazinePage(arr)}
        icon={<AddIcon className='' sx={{ fontSize: 40 }} />}
      />
    )
  }

  const [loadingImage, isLoadingImage] = useState(false)

  function deleteMagazinePage (idx) {
    const manipulateQuery = { ...addContentQuery }
    const manipulateQueryProduct = [...manipulateQuery.product]

    manipulateQueryProduct.splice(idx, 1)

    manipulateQuery.product = manipulateQueryProduct
    setAddContentQuery(manipulateQuery)
    // const manipulateQueryIndex = { ...manipulateQueryProduct[idx] }
  }

  function displayMagazine (data, idx, arr) {
    if (addContentQuery?.product[idx].image_url === '') {
      return (
        <>
          <Input
            additionalClass='magazine-input'
            type='secondaryActionType' placeholder='Input Magazine ImageUrl'
            additionalParams={idx}
            additionalSecondParams='image'
            handleOnChange={changeInputValue}
            width='70%'
            height='10%'
          />
          {arr.length > 2 &&
            <Button
              value='Delete'
              type='delete'
              additionalClass='margin-top-xs'
              fontSize='m'
              name='campaign-header'
              fontWeight='bold'
              width='70%'
              height='7%'
              handleOnClick={() => deleteMagazinePage(idx)}
              icon={<DeleteIcon sx={{ color: '#FEFEFC', fontSize: '1.5em' }} />}
            />}
        </>
      )
    } else {
      return (
        <>
          {popupAction(addContentQuery?.product[idx].image_url, idx)}
          <CustomLazyLoadImage
            loadingImage={loadingImage}
            isLoadingImage={isLoadingImage}
            prevIsLoading
            image={addContentQuery?.product[idx].image_url}
          />
        </>
      )
    }
  }

  // https://res.cloudinary.com/donu8hbzs/image/upload/v1656513928/Content%20Management%20System/Magazine/Story%20Of%20Virgil/1_j0wvfs.png
  const popupAction = (image, idx) => {
    return (
      <div className='popUp-action center'>
        <Button
          type='transparent'
          additionalClass='increment-button'
          size='s'
          fontSize='s'
          name='campaign-header'
          fontWeight='bold'
          height='unset'
          width=''
          handleOnClick={() => changeInputValue('', idx, 'delete')}
          icon={<DeleteIcon sx={{ color: '#8F0900', fontSize: '3em' }} />}
        />
      </div>
    )
  }

  function renderBody (params) {
    const { addContentQuery } = context

    const settings = {
      slidesToScroll: addContentQuery?.product.length > 1 ? 2 : 1,
      infinite: false,
      // autoplay: true,
      // autoplaySpeed: 7000,
      dots: true,
      // pauseOnHover: true,
      slidesToShow: addContentQuery?.product.length > 1 ? 2 : 1
    }

    return (
      <>
        <div className='magazine-container-body-body margin-right-l'>
          <Slider {...settings}>
            {addContentQuery.product.map((data, idx, arr) => (
              <div key={idx}>
                <div className='magazine-container-body-body-slick center column margin-left-l'>
                  {magazineBody(data, idx, arr)}
                </div>
              </div>
            ))}
          </Slider>
        </div>

        <div className='magazine-container-body-title margin-left-xxxl'>
          <Input
            type='transparent'
            placeholder='Input Magazine Title'
            additionalClass='margin-top-xl'
            height='10%'
            handleOnChange={changeInputValue}
            additionalSecondParams='title'
            value={addContentQuery?.title}
          />
          <JoditEditor
            value={content}
            config={config.editorConfig}
            onBlur={newContent => content !== '' && setContent(newContent)}
          />
        </div>
      </>
    )
  }
  return (
    <>
      <div className='magazine-container-header center'>
        {renderHeader()}
      </div>

      <div className='magazine-container-body padding-left-xxxl'>
        {renderBody()}
      </div>
    </>
  )
}
