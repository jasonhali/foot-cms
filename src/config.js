const config = {
  DEBUG: false,
  imageRR: 'https://res.cloudinary.com/ruparupa-com/image/upload/',
  baseUrl: 'http://localhost:3000/',
  navigationUrl: 'http://localhost:3000',
  appPassword: '9MCw9rDJjZ4VJ2zNwBLZ',
  imageUrlDummy: 'https://drive.google.com/uc?export=view&id=1fDDr6mpIpH7D7HgmL2Tj56Vh3cd_OUfl',
  imageUrl: 'https://drive.google.com/uc?export=view&id=',
  assetsURL: 'http://localhost:3000/Asset/',
  driveUrl: 'https://drive.google.com/drive/u/0/folders/1EmqZA7vFQDYS4TmyxujnoIGn8reTHpzW',

  editorConfig: {
    // toolbar: true,
    buttons: ['bold', 'strikethrough', 'underline', 'italic', 'eraser', '|', 'fontsize', 'align', 'ol', 'brush', '|', 'image', 'video', '|', '|', 'hr', 'symbol'],
    showCharsCounter: false,
    showWordsCounter: false,
    showXPathInStatusbar: false
  },

  editorConfig2: {
    // toolbar: true,
    buttons: ['bold', 'strikethrough', 'underline', 'italic', 'eraser', '|', 'fontsize', 'align', 'ol', 'brush', '|', 'image', 'video', '|', '|', 'hr', 'symbol'],
    showCharsCounter: false,
    showWordsCounter: false,
    showXPathInStatusbar: false
  }
}

export default config
// https://drive.google.com/uc?export=view&id=
// di tambahi id dari link gdrive

// https://drive.google.com/uc?export=view&id=1H7zsNoKz96SLl4jmst5glEOUh3VU_gjd

// https://drive.google.com/file/d/1c9XAd_ESas8Yd4mEsjVxLSSrcl1xWZpd/view?usp=sharing
